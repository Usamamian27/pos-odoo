import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import PrivateRoute from "./components/common/PrivateRoute";

//layout
import Sidebar from "./components/layouts/Sidebar";
import Dashboard from "./components/layouts/Dashboard";
import Navbar from "./components/layouts/Navbar";

//products
import AddProduct from "./components/products/AddProduct";
import EditProduct from "./components/products/EditProduct";
import Products from "./components/products/Products";
import Product from "./components/products/Product";

//category
import AddCategory from "./components/category/AddCategory";
import EditCategory from "./components/category/EditCategory";
import Categories from "./components/category/Categories";

//  Order
import Orders from "./components/orders/Orders";
import Order from "./components/orders/Order";

// Retailer Dashboard Items
import Sell from "./components/Retailer/Sell";
import Pad from "./components/Retailer/Pad";
import Receipt from "./components/Retailer/Receipt";
import Report from "./components/reports/Report";

// Charts
import Chart from "./components/reports/Charts";

import { getOrders } from "./actions/orderActions";

class Wrapper extends Component {
  constructor() {
    super();
    this.state = {
      chartData: {}
    };
  }

  componentWillMount() {
    this.getChartData();
    this.props.getOrders();
  }

  getChartData() {
    // Ajax calls here
    this.setState({
      chartData: {
        labels: [
          "Boston",
          "Worcester",
          "Springfield",
          "Lowell",
          "Cambridge",
          "New Bedford"
        ],
        datasets: [
          {
            label: "Population",
            data: [617594, 181045, 153060, 106519, 105162, 95072],
            backgroundColor: [
              "rgba(255, 99, 132, 0.6)",
              "rgba(54, 162, 235, 0.6)",
              "rgba(255, 206, 86, 0.6)",
              "rgba(75, 192, 192, 0.6)",
              "rgba(153, 102, 255, 0.6)",
              "rgba(255, 159, 64, 0.6)",
              "rgba(255, 99, 132, 0.6)"
            ]
          }
        ]
      }
    });
  }
  render() {
    return (
      <div id="wrapper">
        <Route path="/" component={Sidebar} />

        <div id="content-wrapper" className="d-flex flex-column">
          <div id="content">
            <Route path="/" component={Navbar} />
            <Route exact path="/" component={Dashboard} />
            {/* <Chart
              chartData={this.state.chartData}
              location="Massachusetts"
              legendPosition="bottom"
            /> */}
            <PrivateRoute exact path="/add-product" component={AddProduct} />

            <PrivateRoute exact path="/edit-product" component={EditProduct} />
            <PrivateRoute exact path="/products" component={Products} />
            <Switch>
              <PrivateRoute exact path="/product" component={Product} />
            </Switch>

            <PrivateRoute exact path="/add-category" component={AddCategory} />
            <PrivateRoute
              exact
              path="/edit-category"
              component={EditCategory}
            />
            <PrivateRoute exact path="/categories" component={Categories} />

            <PrivateRoute exact path="/orders" component={Orders} />
            <PrivateRoute exact path="/order" component={Order} />

            <PrivateRoute exact path="/sell" component={Sell} />
            <PrivateRoute exact path="/pad" component={Pad} />
            <PrivateRoute exact path="/receipt" component={Receipt} />
            <PrivateRoute exact path="/daily" component={Report} />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});
export default connect(
  mapStateToProps,
  { getOrders }
)(Wrapper);
