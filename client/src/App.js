import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import { Provider } from "react-redux";

import jwt_decode from "jwt-decode";
import setAuthToken from "./components/common/setAuthToken";
import { setCurrentUser, logoutUser } from "./actions/authActions";

import store from "./store";

//auth
import Login from "./components/auth/Login";
// import Register from "./components/auth/Register";
import ForgotPassword from "./components/auth/ForgotPassword";

import Wrapper from "./Wrapper";

import "./App.css";

// Check for token
if (localStorage.jwtToken) {
  // Set Auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode Token
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set User and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Switch>
              {/* <Route exact path="/register" component={Register} /> */}
              <Route exact path="/login" component={Login} />
              <Route exact path="/forgot-password" component={ForgotPassword} />

              <Route component={Wrapper} />
            </Switch>
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;
