import axios from "axios";

import {
  GET_ERRORS,
  GET_PRODUCTS,
  GET_PRODUCT,
  //   CLEAR_CURRENT_PRODUCT,
  PRODUCT_LOADING,
  DELETE_PRODUCT,
  ADDTOCART
} from "./types";

// ADD Product
export const addProduct = (newProduct, history) => dispatch => {
  axios

    .post("/api/product/create", newProduct)
    .then(res => history.push("/products"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const searchProduct = (searchValue, history) => dispatch => {
  axios

    .get(`/api/search/product/${searchValue}`)
    .then(res =>
      dispatch({
        type: GET_PRODUCTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setDataToState = arr => {
  return {
    type: ADDTOCART,
    payload: arr
  };
};

//  Get All Products
export const getProducts = () => dispatch => {
  dispatch(setProductLoading());
  axios
    .get("/api/products")
    .then(res =>
      dispatch({
        type: GET_PRODUCTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PRODUCTS,
        payload: null
      })
    );
};

// Get Product by Id
export const getProductById = id => dispatch => {
  dispatch(setProductLoading());
  axios
    .get(`/api/get-product/${id}`)
    .then(res =>
      dispatch({
        type: GET_PRODUCT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PRODUCT,
        payload: null
      })
    );
};

// Update Product By Id
export const updateProduct = (id, updatedProduct, history) => dispatch => {
  axios
    .post(`/api/product/update/${id}`, updatedProduct)
    .then(res => history.push("/products"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Product By Id
export const deleteProduct = (id, history) => dispatch => {
  axios
    .delete(`/api/product/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_PRODUCT,
        payload: res.data
      });
      history.push("/products");
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err
      })
    );
};

// Set Product Loading
export const setProductLoading = () => {
  return {
    type: PRODUCT_LOADING
  };
};
