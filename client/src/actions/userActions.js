import axios from "axios";

import {
  GET_ERRORS,
  GET_USERS,
  GET_USER,
  SET_USER,
  //   CLEAR_CURRENT_USER,
  USER_LOADING,
  DELETE_USER
} from "./types";

// ADD USER
export const addUser = (newUser, history) => dispatch => {
  axios
    .post("http://198.245.53.50:3000/api/users/add", newUser)
    .then(res => history.push("/users"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//  Get All USERS
export const getUsers = () => dispatch => {
  dispatch(setUserLoading());
  axios
    .get("http://198.245.53.50:3000/api/users")
    .then(res =>
      dispatch({
        type: GET_USERS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_USERS,
        payload: null
      })
    );
};

// Update User By Id
export const updateUser = (id, updatedUser, history) => dispatch => {
  // console.log(updatedUser);
  axios
    .put(`http://198.245.53.50:3000/api/users/${id}`, updatedUser)
    .then(res => history.push("/users"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get User By Id
export const getUserById = id => dispatch => {
  axios
    .get(`http://198.245.53.50:3000/api/users/${id}`)
    .then(res =>
      dispatch({
        type: GET_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete User By Id
export const deleteUser = id => dispatch => {
  //   console.log(id);
  axios
    .delete(`http://198.245.53.50:3000/api/users/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setUserToReduxState = user => dispatch => {
  dispatch({
    type: SET_USER,
    payload: user
  });
};

// // // Get All Sub-Categories
// // export const getSubCategories = () => dispatch => {
// //   dispatch(setCategoryLoading());
// //   axios
// //     .get("http://198.245.53.50:3000/api/category")
// //     .then(res =>
// //       dispatch({
// //         type: GET_CATEGORIES,
// //         payload: res.data
// //       })
// //     )
// //     .catch(err =>
// //       dispatch({
// //         type: GET_CATEGORIES,
// //         payload: null
// //       })
// //     );
// // };

// Set User Loading True
export const setUserLoading = () => {
  return {
    type: USER_LOADING
  };
};

// // // Clear Current Profile
// // export const clearCurrentCategory = () => {
// //   return {
// //     type: CLEAR_CURRENT_CATEGORY
// //   };
// // };
