export const GET_ERRORS = "GET_ERRORS";
export const CLEAR_ERRORS = "CLEAR_ERRORS";
export const SET_CURRENT_USER = "SET_CURRENT_USER";
export const GET_PROFILE = "GET_PROFILE";
export const PROFILE_LOADING = "PROFILE_LOADING";
export const PROFILE_NOT_FOUND = "PROFILE_NOT_FOUND";
export const CLEAR_CURRENT_PROFILE = "CLEAR_CURRENT_PROFILE";
export const GET_PROFILES = "GET_PROFILES";
export const POST_LOADING = "POST_LOADING";
export const GET_POSTS = "GET_POSTS";
export const GET_POST = "GEOSTLES";
export const ADD_POST = "ADD_POST";
export const DELETE_POST = "DELETE_POST";

// Category
export const GET_CATEGORY = "GET_CATEGORY";
export const GET_CATEGORIES = "GET_CATEGORIES";
export const CATEGORY_LOADING = "CATEGORY_LOADING";
export const CLEAR_CURRENT_CATEGORY = "CLEAR_CURRENT_CATEGORY";
export const DELETE_CATEGORY = "DELETE_CATEGORY";
// export const UPDATE_CATEGORY = "UPDATE_CATEGORY";

// Product
export const GET_PRODUCT = "GET_PRODUCT";
export const GET_PRODUCTS = "GET_PRODUCTS";
export const PRODUCT_LOADING = "PRODUCT_LOADING";
export const CLEAR_CURRENT_PRODUCT = "CLEAR_CURRENT_PRODUCT";
export const DELETE_PRODUCT = "DELETE_PRODUCT";

// Role
export const GET_ROLES = "GET_ROLES";
export const GET_ROLE = "GET_ROLE";
export const ROLE_LOADING = "ROLE_LOADING";
export const CLEAR_CURRENT_ROLE = "CLEAR_CURRENT_ROLE";
export const DELETE_ROLE = "DELETE_ROLE";

// Permission
export const GET_PERMISSIONS = "GET_PERMISSIONS";
export const PERMISSIONS_LOADING = "PERMISSIONS_LOADING";

// For Admin Panel User excluding Super Admin
export const GET_USER = "GET_USER";
export const SET_USER = "SET_USER"; // for setting user in state
export const GET_USERS = "GET_USERS";
export const USER_LOADING = "USER_LOADING";
export const CLEAR_CURRENT_USER = "CLEAR_CURRENT_USER";
export const DELETE_USER = "DELETE_USER";

// Vendors
export const GET_VENDOR = "GET_VENDOR";
export const SET_VENDOR = "SET_VENDOR"; // for setting VENDOR in state
export const GET_VENDORS = "GET_VENDORS";
export const VENDOR_LOADING = "VENDOR_LOADING";
export const CLEAR_CURRENT_VENDOR = "CLEAR_CURRENT_VENDOR";
export const DELETE_VENDOR = "DELETE_VENDOR";

// Customers
export const GET_CUSTOMER = "GET_CUSTOMER";
export const SET_CUSTOMER = "SET_CUSTOMER"; // for setting CUSTOMER in state
export const GET_CUSTOMERS = "GET_CUSTOMERS";
export const CUSTOMER_LOADING = "CUSTOMER_LOADING";
export const CLEAR_CURRENT_CUSTOMER = "CLEAR_CURRENT_CUSTOMER";
export const DELETE_CUSTOMER = "DELETE_CUSTOMER";
export const GET_CURRENT_USER_PROFILE = "GET_CURRENT_USER_PROFILE";

// Orders
export const GET_ORDER = "GET_ORDER";
export const SET_ORDER = "SET_ORDER"; // for setting ORDER in state
export const GET_ORDERS = "GET_ORDERS";
export const ORDER_LOADING = "ORDER_LOADING";
export const CLEAR_CURRENT_ORDER = "CLEAR_CURRENT_ORDER";
export const DELETE_ORDER = "DELETE_ORDER";
export const GET_BIDS = "GET_BIDS";
export const BIDS_LOADING = "BIDS_LOADING";

// Coupons
export const COUPONS_LOADING = "COUPONS_LOADING";
export const GET_COUPONS = "GET_COUPONS";
export const DELETE_COUPON = "DELETE_COUPON";

// Payments For Vendor
export const GET_PAYMENT_HISTORY = "GET_PAYMENT_HISTORY";
export const DELETE_PAYMENT_HISTORY = "DELETE_PAYMENT_HISTORY";

export const ADDTOCART = "ADDTOCART";
