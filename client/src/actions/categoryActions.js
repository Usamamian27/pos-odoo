import axios from "axios";

import {
  GET_ERRORS,
  GET_CATEGORIES,
  CLEAR_CURRENT_CATEGORY,
  CATEGORY_LOADING,
  DELETE_CATEGORY
} from "./types";

// ADD CATEGORY

export const addCategory = (newCategory, history) => dispatch => {
  axios
    .post("/api/category/create", newCategory)
    .then(res => history.push("/categories"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get All Categories
export const getCategories = () => dispatch => {
  dispatch(setCategoryLoading());
  axios
    .get("/api/categories")
    .then(res =>
      dispatch({
        type: GET_CATEGORIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_CATEGORIES,
        payload: null
      })
    );
};

// Update Category By Id
export const updateCategory = (id, newCategory, history) => dispatch => {
  axios
    .put(`/api/category/${id}`, newCategory)
    .then(res => history.push("/categories"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Delete Category By Id
export const deleteCategory = id => dispatch => {
  axios
    .delete(`/api/category/${id}`)
    .then(res =>
      dispatch({
        type: DELETE_CATEGORY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get All Sub-Categories
export const getSubCategories = () => dispatch => {
  dispatch(setCategoryLoading());
  axios
    .get("/api/category")
    .then(res =>
      dispatch({
        type: GET_CATEGORIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_CATEGORIES,
        payload: null
      })
    );
};

// Profile Loading
export const setCategoryLoading = () => {
  return {
    type: CATEGORY_LOADING
  };
};

// Clear Current Profile
export const clearCurrentCategory = () => {
  return {
    type: CLEAR_CURRENT_CATEGORY
  };
};
