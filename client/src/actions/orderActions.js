import axios from "axios";

import {
  GET_ERRORS,
  GET_ORDERS,
  GET_BIDS,
  // GET_ORDER,
  //   SET_ORDER,
  //   CLEAR_CURRENT_ORDER,
  ORDER_LOADING,
  BIDS_LOADING
  //   DELETE_ORDER
} from "./types";

//  Get All Orders
export const getOrders = () => dispatch => {
  dispatch(setOrderLoading());
  axios
    .get("/api/orders")
    .then(res =>
      dispatch({
        type: GET_ORDERS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ORDERS,
        payload: null
      })
    );
};

// Place Order
export const PlaceOrder = (newOrder, history) => dispatch => {
  console.log(newOrder);
  axios
    // http://198.245.53.50:3000/api/product/create
    .post("/api/order/create", newOrder)
    .then(res =>
      history.push({ pathname: "/receipt", state: { order: res.data } })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Get Customer's Orders By His Id
export const getOrdersByCustomerId = id => dispatch => {
  console.log(id);
  axios
    .get(`/api/universal/orders/${id}`)
    .then(res =>
      dispatch({
        type: GET_ORDERS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

//  Get All Vendor Bids on single Order
export const getVendorBidsOnOrder = id => dispatch => {
  dispatch(setBidsLoading());
  axios
    .get(`/api/bids/${id}`)
    .then(res =>
      dispatch({
        type: GET_BIDS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_BIDS,
        payload: null
      })
    );
};

// Assign order to vendor assign/:id/:v_id
export const assignOrderToVendor = (o_Id, v_Id, history) => dispatch => {
  // console.log(v_Id, o_Id);
  axios
    .post(`/api/assign/${o_Id}/${v_Id}`)
    .then(res => history.push("/"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Notify vendor /:order_id/:vendor_id
export const notifyVendor = (o_Id, v_Id, history) => dispatch => {
  // console.log(v_Id, o_Id);
  axios
    .post(`/api/notify/vendor/${o_Id}/${v_Id}`)
    .then(res => history.push("/"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Shipp order to customer
export const shipOrderToCustomer = (o_Id, newShipment, history) => dispatch => {
  console.log(newShipment, o_Id);
  axios
    .post(`/api/ship/order/customer/${o_Id}`, newShipment)
    .then(res => history.push("/shipped"))
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// // Update User By Id
// export const updateUser = (id, updatedUser, history) => dispatch => {
//   // console.log(updatedUser);
//   axios
//     .put(`http://198.245.53.50:3000/api/users/${id}`, updatedUser)
//     .then(res => history.push("/users"))
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

// // Get User By Id
// export const getUserById = id => dispatch => {
//   axios
//     .get(`http://198.245.53.50:3000/api/users/${id}`)
//     .then(res =>
//       dispatch({
//         type: GET_USER,
//         payload: res.data
//       })
//     )
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

// // Delete User By Id
// export const deleteUser = id => dispatch => {
//   //   console.log(id);
//   axios
//     .delete(`http://198.245.53.50:3000/api/users/${id}`)
//     .then(res =>
//       dispatch({
//         type: DELETE_USER,
//         payload: res.data
//       })
//     )
//     .catch(err =>
//       dispatch({
//         type: GET_ERRORS,
//         payload: err.response.data
//       })
//     );
// };

// export const setUserToReduxState = user => dispatch => {
//   dispatch({
//     type: SET_USER,
//     payload: user
//   });
// };

// // Get All Sub-Categories
// export const getSubCategories = () => dispatch => {
//   dispatch(setCategoryLoading());
//   axios
//     .get("http://198.245.53.50:3000/api/category")
//     .then(res =>
//       dispatch({
//         type: GET_CATEGORIES,
//         payload: res.data
//       })
//     )
//     .catch(err =>
//       dispatch({
//         type: GET_CATEGORIES,
//         payload: null
//       })
//     );
// };

// Set Customer Loading True
export const setOrderLoading = () => {
  return {
    type: ORDER_LOADING
  };
};

// Set Bids Loading True
export const setBidsLoading = () => {
  return {
    type: BIDS_LOADING
  };
};

// // Clear Current Profile
// export const clearCurrentCategory = () => {
//   return {
//     type: CLEAR_CURRENT_CATEGORY
//   };
// };
