import {
  GET_PRODUCT,
  PRODUCT_LOADING,
  GET_PRODUCTS,
  CLEAR_CURRENT_PRODUCT,
  DELETE_PRODUCT,
  ADDTOCART
} from "../actions/types";
// import isEmpty from "../components/common/is-empty";

const initialState = {
  loading: false,
  product: {},
  products: [],
  cart: []
};

export default function(state = initialState, action) {
  switch (action.type) { 
    case PRODUCT_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_PRODUCTS:
      return {
        ...state,
        products: action.payload,
        loading: false
      };
    case GET_PRODUCT:
      return {
        ...state,
        product: action.payload,
        loading: false
      };
    case CLEAR_CURRENT_PRODUCT:
      return {
        ...state,
        product: null
      };
    case DELETE_PRODUCT:
      return {
        ...state,
        products: state.products.filter(
          product => product._id !== action.payload._id
        )
      };
    case ADDTOCART:
      return {
        ...state,
        cart: action.payload,
        loading: false
      };
    default:
      return state;
  }
}
