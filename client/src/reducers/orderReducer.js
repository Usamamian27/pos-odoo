import {
  GET_ORDER,
  SET_ORDER,
  ORDER_LOADING,
  GET_ORDERS,
  CLEAR_CURRENT_ORDER,
  DELETE_ORDER,
  GET_BIDS,
  BIDS_LOADING
} from "../actions/types";
// import isEmpty from "../components/common/is-empty";

const initialState = {
  loading: false,
  bidsLoading: false,
  order: {},
  orders: [],
  bids: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ORDER_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_ORDERS:
      return {
        ...state,
        orders: action.payload,
        loading: false
      };
    case GET_ORDER:
      return {
        ...state,
        order: action.payload
      };
    case SET_ORDER:
      return {
        ...state,
        order: action.payload
      };
    case CLEAR_CURRENT_ORDER:
      return {
        ...state,
        order: null
      };
    case DELETE_ORDER:
      return {
        ...state,
        orders: state.orders.filter(order => order._id !== action.payload._id)
      };
    case BIDS_LOADING:
      return {
        ...state,
        bidsLoading: true
      };
    case GET_BIDS:
      return {
        ...state,
        bids: action.payload,
        bidsLoading: false
      };
    default:
      return state;
  }
}
