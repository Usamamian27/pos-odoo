import {
  GET_CATEGORY,
  CATEGORY_LOADING,
  GET_CATEGORIES,
  CLEAR_CURRENT_CATEGORY,
  DELETE_CATEGORY
} from "../actions/types";
// import isEmpty from "../components/common/is-empty";

const initialState = {
  loading: false,
  category: {},
  categories: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CATEGORY_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_CATEGORIES:
      return {
        ...state,
        categories: action.payload,
        loading: false
      };
    case GET_CATEGORY:
      return {
        ...state,
        category: action.payload
      };
    case CLEAR_CURRENT_CATEGORY:
      return {
        ...state,
        category: null
      };
    case DELETE_CATEGORY:
      return {
        ...state,
        categories: state.categories.filter(
          category => category._id !== action.payload._id
        )
      };
    default:
      return state;
  }
}
