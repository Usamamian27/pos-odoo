import { SET_CURRENT_USER, GET_CURRENT_USER_PROFILE } from "../actions/types";
import isEmpty from "../components/common/is-empty";

const initialState = {
  isAuthenticated: false,
  profile: {},
  user: {}
};

export default function(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !isEmpty(action.payload),
        user: action.payload
      };
    case GET_CURRENT_USER_PROFILE:
      return {
        ...state,
        profile: action.payload
      };
    default:
      return state;
  }
}
