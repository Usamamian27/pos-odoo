import React, { Component } from "react";
import classnames from "classnames";

class BootstrapCarousel extends Component {
  render() {
    let { photoUrl } = this.props;

    photoUrl = photoUrl[0].split(",");

    let carouselItem = photoUrl.map(url => (
      <div
        className={
          "carousel-item " +
          classnames({
            active: photoUrl[0] === url,
            "": false
          })
        }
        key={url}
      >
        <img
          className="img-fluid"
          src={url}
          alt="Los Angeles"
          style={{ height: 300 }}
        />
      </div>
    ));

    return (
      <div id="demo" className="carousel slide" data-ride="carousel">
        {/* Indicators */}
        <ul className="carousel-indicators">
          <li data-target="#demo" data-slide-to={0} className="active" />
          <li data-target="#demo" data-slide-to={1} />
          <li data-target="#demo" data-slide-to={2} />
        </ul>

        {/* The slideshow */}
        <div className="carousel-inner">{carouselItem}</div>

        {/* Left and right controls */}
        <a className="carousel-control-prev" href="#demo" data-slide="prev">
          <span className="carousel-control-prev-icon" />
        </a>
        <a className="carousel-control-next" href="#demo" data-slide="next">
          <span className="carousel-control-next-icon" />
        </a>
      </div>
    );
  }
}
export default BootstrapCarousel;
