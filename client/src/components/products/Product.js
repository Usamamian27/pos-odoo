import React, { Component } from "react";
import Carousel from "../common/Carousel";
import { Link, withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { getProductById, deleteProduct } from "../../actions/productActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";

class Product extends Component {
  constructor(props) {
    super(props);
    this.state = {
      product: null
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.product.product === null && this.props.product.loading) {
      this.props.history.push("/not-found");
    }
  }
  onDeleteProduct = id => {
    this.props.deleteProduct(id, this.props.history);
  };

  render() {
    const { product } = this.props.location.state;
    const { loading } = this.props.product;

    let data = null;

    if (product === null || loading) {
      //   data = <Spinner />;
    } else {
      if (!isEmpty(product)) {
        data = (
          <div className="row justify-content-center m-2 p-2">
            <div className="col-md-8 col-sm-10 col-lg-6 col-xl-4 col-xs-10">
              <Carousel photoUrl={product.photoUrl} />
            </div>

            <div className="col-md-8 col-sm-10 col-lg-6 col-xl-6 col-xs-10">
              <span className="float-right">
                <Link
                  to={{ pathname: "/edit-product", state: { item: product } }}
                  className="btn btn-info"
                >
                  <i className="fas fa-edit"> Edit</i>
                </Link>
                <button
                  onClick={() => this.onDeleteProduct(product._id)}
                  // to="/products"
                  className="btn btn-danger"
                >
                  <i className="fas fa-trash"> Delete</i>
                </button>
              </span>
              <div>
                <h3 className="product-title">{product.name}</h3>
                <p className="product-description">
                  Suspendisse quos? Tempus cras iure temporibus? Eu laudantium
                  cubilia sem sem! Repudiandae et! Massa senectus enim minim
                  sociosqu delectus posuere.
                </p>
                <p>
                  Price:{" "}
                  <span className="text-success">
                    Rs.{parseFloat(product.salesPrice).toFixed(2)}
                  </span>
                </p>

                <p>
                  Type:{" "}
                  <span className="text-medium">{product.productType}</span>
                </p>
                {/* <p>
                    Colors :{" "}
                    <strong>
                      {product.colors ? (
                        <span>
                          {product.colors.map(s => (
                            <span key={s}>
                              <span
                                className="btn btn-info"
                                key={s}
                                style={{ borderRadius: 50 }}
                              >
                                {s}
                              </span>
                              {"\u00A0"} {"\u00A0"}
                            </span>
                          ))}
                        </span>
                      ) : (
                        "Size not selected"
                      )}
                    </strong>
                  </p> */}
                {/* <p>
                    Stock : <strong>{product.stock}</strong>
                  </p> */}
              </div>
            </div>
          </div>
        );
      }
    }

    return (
      <div className="row justify-content-center m-1 p-1">
        <div className="col-md-10">
          <div className="card mb-4">
            <div className="card-header">
              Product <strong>Information</strong>
              {/* <span className="float-right">
                <Link
                  to={{ pathname: "/edit-product", state: { item: product } }}
                  className="btn btn-info"
                >
                  <i className="fas fa-edit"> Edit</i>
                </Link>
              </span> */}
            </div>
            <br />
            <br />

            {loading ? <Spinner /> : <span> {data}</span>}

            <div className="card-footer" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product
});

export default connect(
  mapStateToProps,
  { getProductById, deleteProduct }
)(withRouter(Product));
