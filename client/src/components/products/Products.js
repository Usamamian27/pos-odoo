import React, { Component } from "react";

import { MDBDataTable } from "mdbreact";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import Spinner from "../common/Spinner";
// import isEmpty from "../common/is-empty";
import { getProducts } from "../../actions/productActions";
// import ProductItem from "./ProductItem";

class Products extends Component {
  componentDidMount() {
    this.props.getProducts();
  }

  handleRowClick(product) {
    this.props.history.push({
      pathname: "/product",
      state: { product: product }
    });
  }

  render() {
    let { products, loading } = this.props.product;

    let productsBody = {
      columns: [
        {
          label: "#",
          field: "id"
          //   sort: "asc",
          //   width: 150,
        },
        {
          label: "Product Name",
          field: "product"
          //   sort: "asc",
          // width: 150
        },
        {
          label: "Product Type",
          field: "customize"
          //   sort: "asc",
          // width: 270
        },
        {
          label: "Price",
          field: "price"
        }
      ],
      rows: []
    };

    let i = 0;

    if (products === null || loading) {
    } else {
      if (products.length > 0) {
        products.map(product =>
          productsBody.rows.push({
            id: ++i,
            product: product.name,
            customize: product.productType,
            price: (
              <span className="text-success">
                Rs.{parseFloat(product.salesPrice).toFixed(2)}
              </span>
            ),
            clickEvent: this.handleRowClick.bind(this, product)
          })
        );
      }
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Products</strong> Information
              <span className="float-right">
                <Link to="/add-product" className="btn btn-info">
                  <i className="fas fa-plus" /> Add Product
                </Link>
              </span>
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container ">
                <MDBDataTable
                  // striped
                  responsive
                  hover
                  theadColor="#FFAA"
                  tbodyColor="#FFFF"
                  bordered
                  className="text-center"
                  data={productsBody}
                />
              </div>
            )}
            <div className="card-footer">
              {/* <strong>Card</strong> Footer */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product
});

export default connect(
  mapStateToProps,
  { getProducts }
)(Products);
