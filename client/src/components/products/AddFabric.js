import React, { Component } from "react";
import TextFieldGroup from "../common/TextFieldGroup";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import { addFabric } from "../../actions/productActions";
import isEmpty from "../common/is-empty";

class AddFabric extends Component {
  state = {
    fabricName: "",
    fabricPhotoUrl: "",
    fabricPrice: "",
    product_id: "",
    file: []
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  setImages = e => {
    let File = [];
    for (let i = 0; i < e.target.files.length; i++) {
      File.push(e.target.files[i]);
    }
    this.setState({
      file: File
    });
  };
  componentDidMount() {
    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      this.props.history.push("/products");
    } else {
      const { item } = this.props.location.state;

      // console.log(item);
      this.setState({ product_id: item._id });
    }
  }

  onSubmit = e => {
    e.preventDefault();
    const fabricDetails = {
      fabricName: this.state.fabricName,
      fabricPrice: this.state.fabricPrice
    };

    let myFiles = this.state.file;
    const fd = new FormData();
    for (let i = 0; i < myFiles.length; i++) {
      fd.append("myFiles", myFiles[i], myFiles[i].name);
    }
    let url = "";
    axios.post("http://198.245.53.50:3000/api/uploadmultiple", fd).then(res => {
      for (let i = 0; i < res.data.length; i++) {
        url += res.data[i] + ",";
      }
      url = url.substring(0, url.length - 1);
      console.log(url);
      if (!isEmpty(url)) {
        this.setState({ fabricPhotoUrl: url });
        fabricDetails.fabricPhotoUrl = this.state.fabricPhotoUrl;
        this.props.addFabric(
          this.state.product_id,
          fabricDetails,
          this.props.history
        );
      } else {
        setTimeout(5000);
        this.setState({ fabricPhotoUrl: url });
        fabricDetails.fabricPhotoUrl = this.state.fabricPhotoUrl;
        this.props.addFabric(
          this.state.product_id,
          fabricDetails,
          this.props.history
        );
      }
    });

    console.log("fabric details ", fabricDetails);
  };
  render() {
    const { errors } = this.state;

    return (
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">
              <strong>Add</strong> Fabric
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={this.onSubmit}
                encType="multipart/form-data"
                style={{ marginLeft: "30px" }}
              >
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Fabric Name
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Fabric Name"
                      name="fabricName"
                      value={this.state.fabricName}
                      onChange={this.onChange}
                      //   error={errors.name}
                      //   info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Fabric Price
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Price of fabric"
                      name="fabricPrice"
                      type="number"
                      value={this.state.fabricPrice}
                      onChange={this.onChange}
                      //   error={errors.fabricPrice}
                      //   info=""
                    />
                  </div>
                </div>

                <div className="form-group">
                  <input type="file" name="myFiles" onChange={this.setImages} />
                </div>

                <div
                  className="form-group row float-right card-footer"
                  style={{ marginRight: "10px" }}
                >
                  <button className="btn btn-sm btn-primary" type="submit">
                    <i className="fa fa-dot-circle-o" /> Add Fabric
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(
  null,
  { addFabric }
)(withRouter(AddFabric));
