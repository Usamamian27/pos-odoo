import React, { Component } from "react";
import { Link } from "react-router-dom";
import axios from "axios";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import { getCategories } from "../../actions/categoryActions";
import { addProduct } from "../../actions/productActions";

class AddProduct extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      salesPrice: "",
      productType: "",
      barCode: "",
      expiryDate: "",
      // colors: "",
      // size: "",
      // stock: "",
      // XLStock: "",
      // LStock: "",
      // XSStock: "",
      // MStock: "",
      // SStock: "",
      category: null,
      selectedCategory: "",
      file: [],
      errors: {}
    };
  }

  componentDidMount() {
    this.props.getCategories();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors.errors) {
      this.setState({
        errors: nextProps.errors.errors
      });
    } else {
      this.setState({
        errors: ""
      });
    }
  }

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  setImages = e => {
    let file = [];
    for (let i = 0; i < e.target.files.length; i++) {
      file.push(e.target.files[i]);
    }

    this.setState({
      file: file
    });
  };

  onSubmit = e => {
    e.preventDefault();

    let getSizes = "";

    for (var ref in this.refs) {
      if (this.refs[ref].checked) getSizes += this.refs[ref].name + ",";
    }
    getSizes = getSizes.slice(0, -1);

    const newProduct = {
      name: this.state.name,
      salesPrice: this.state.salesPrice,
      productType: this.state.productType,
      barCode: this.state.barCode,
      expiryDate: this.state.expiryDate,

      // colors: this.state.colors,
      // size: getSizes,
      // // stock: this.state.stock,
      // XLStock: this.state.XLStock,
      // LStock: this.state.LStock,
      // XSStock: this.state.XSStock,
      // MStock: this.state.MStock,
      // SStock: this.state.SStock,
      category: this.state.category
    };

    let myFiles = this.state.file;

    if (myFiles.length > 0) {
      const fd = new FormData();

      for (let i = 0; i < myFiles.length; i++) {
        fd.append("myFiles", myFiles[i], myFiles[i].name);
      }

      axios.post("/api/uploadmultiple", fd).then(res => {
        let url = "";

        for (let i = 0; i < res.data.length; i++) {
          url += res.data[i] + ",";
        }

        url = url.substring(0, url.length - 1);

        newProduct.photoUrl = url;
        console.log(newProduct);

        this.props.addProduct(newProduct, this.props.history);
      });
    } else {
      alert("Please select atleast one image");
    }
  };

  onResetClick = e => {
    this.setState({
      name: "",
      salesPrice: "",
      // size: "",
      // stock: "",
      category: "",
      file: [],
      errors: {}
    });
  };

  render() {
    const { categories } = this.props;
    const { errors } = this.state;

    // Select options
    // let options = [
    //   { label: "Select Size of Product", value: 0 },
    //   { label: "Medium", value: "Medium" },
    //   { label: "Large", value: "Large" },
    //   { label: "Extra Large", value: "Extra Large" }
    // ];

    let category = [];

    if (categories.length > 0) {
      const dbCategories = [];
      dbCategories.push({ label: "Select Category of Product", value: "" });
      for (let i = 0; i < categories.length; i++) {
        dbCategories.push({
          label: categories[i].name,
          value: categories[i]._id
        });
      }

      category = dbCategories;
    } else {
      category.push({ label: "No Categories found", value: "" });
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">
              <strong>Add</strong> Product
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={this.onSubmit}
                encType="multipart/form-data"
                style={{ marginLeft: "30px" }}
              >
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Name
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Name of Product"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      error={errors.name}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Sale Price
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Sale Price of Product"
                      type="number"
                      name="salesPrice"
                      value={this.state.salesPrice}
                      onChange={this.onChange}
                      error={errors.salesPrice}
                      info=""
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Product Type
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Product Type of Product"
                      type="text"
                      name="productType"
                      value={this.state.productType}
                      onChange={this.onChange}
                      error={errors.productType}
                      info=""
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Expiry Date
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Expiry Date of Product"
                      type="date"
                      name="expiryDate"
                      value={this.state.expiryDate}
                      onChange={this.onChange}
                      error={errors.expiryDate}
                      info=""
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Bar Code
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="BarCode of Product"
                      type="text"
                      name="barCode"
                      value={this.state.barCode}
                      onChange={this.onChange}
                      error={errors.barCode}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label className="col-md-3 col-form-label" htmlFor="select1">
                    Category
                  </label>
                  <div className="col-md-6">
                    <SelectListGroup
                      name="category"
                      value={this.state.category}
                      onChange={this.onChange}
                      options={category}
                      error={errors.category}
                      info=""
                    />
                  </div>
                  <Link className="col col-form-label" to="/add-category">
                    <i className="fas fa-plus" /> Add
                  </Link>
                  <Link className="col col-form-label" to="/categories">
                    <i className="fas fa-list" /> View
                  </Link>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Upload Image
                  </label>
                  <div className="col-md-6">
                    <input
                      id="image"
                      name="myFiles"
                      type="file"
                      multiple
                      onChange={this.setImages}
                    />
                  </div>
                </div>

                <div
                  className="form-group row justify-content-center card-footer m-2 p-2"
                  // style={{ marginRight: "10px" }}
                >
                  <button className="btn btn-sm btn-primary m-2" type="submit">
                    <i className="fa fa-dot-circle-o" /> Add Product
                  </button>
                  <button
                    className="btn btn-sm btn-danger m-2"
                    onClick={this.onResetClick}
                    type="reset"
                  >
                    <i className="fa fa-ban" /> Reset
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  categories: state.category.categories,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { getCategories, addProduct }
)(withRouter(AddProduct));
