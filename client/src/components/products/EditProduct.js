import React, { Component } from "react";

import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";
import axios from "axios";
import { connect } from "react-redux";
import { updateProduct } from "../../actions/productActions";

class EditProduct extends Component {
  state = {
    id: "",
    name: "",
    salesPrice: "",
    productType: "",
    barCode: "",
    file: [],
    errors: {}
  };

  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  setImages = e => {
    let file = [];
    for (let i = 0; i < e.target.files.length; i++) {
      file.push(e.target.files[i]);
    }

    this.setState({
      file: file
    });
  };

  componentDidMount() {
    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      this.props.history.push("/products");
    } else {
      const { item } = this.props.location.state;

      // console.log(item);

      this.setState({
        id: item._id,
        name: item.name,
        salesPrice: item.salesPrice,
        productType: item.productType,
        barCode: item.barCode
      });
    }
  }

  onSubmit = e => {
    e.preventDefault();

    let updatedProduct = {
      name: this.state.name,
      salesPrice: this.state.salesPrice,
      productType: this.state.productType,
      barCode: this.state.barCode
    };
    let myFiles = this.state.file;

    if (myFiles.length > 0) {
      const fd = new FormData();

      for (let i = 0; i < myFiles.length; i++) {
        fd.append("myFiles", myFiles[i], myFiles[i].name);
      }

      axios.post("/api/uploadmultiple", fd).then(res => {
        let url = "";

        for (let i = 0; i < res.data.length; i++) {
          url += res.data[i] + ",";
        }

        url = url.substring(0, url.length - 1);

        updatedProduct.photoUrl = url;
        console.log(updatedProduct, this.state.id, url);
        this.props.updateProduct(
          this.state.id,
          updatedProduct,
          this.props.history
        );
      });
    } else {
      alert("Please select atleast one image");
    }

    // this.props.updateProduct(this.state.id, updatedProduct, this.props.history);
  };
  render() {
    const { errors } = this.state;

    return (
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">
              <strong>Edit</strong> Product
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={this.onSubmit}
                encType="multipart/form-data"
                style={{ marginLeft: "30px" }}
              >
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Name
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Category Name"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      error={errors.name}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Price
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Price of Product"
                      name="salesPrice"
                      // type="number"
                      value={this.state.salesPrice}
                      onChange={this.onChange}
                      error={errors.salesPrice}
                      info=""
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Product Type
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Type of Product"
                      name="productType"
                      // type="number"
                      value={this.state.productType}
                      onChange={this.onChange}
                      error={errors.productType}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Bar Code
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Barcode of Product"
                      name="barCode"
                      value={this.state.barCode}
                      onChange={this.onChange}
                      error={errors.barCode}
                      info=""
                    />
                  </div>
                </div>
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Upload Image
                  </label>
                  <div className="col-md-6">
                    <input
                      id="image"
                      name="myFiles"
                      type="file"
                      multiple
                      onChange={this.setImages}
                    />
                  </div>
                </div>

                <div
                  className="form-group row float-right card-footer"
                  style={{ marginRight: "10px" }}
                >
                  <button className="btn btn-sm btn-primary" type="submit">
                    <i className="fa fa-dot-circle-o" /> Update Product
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(
  null,
  { updateProduct }
)(EditProduct);
