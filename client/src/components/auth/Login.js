import React, { Component } from "react";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import TextFieldGroup from "../common/TextFieldGroup";
import { loginUser } from "../../actions/authActions";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors.errors) {
      this.setState({ errors: nextProps.errors.errors });
    }
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    const userCredentials = {
      email: this.state.email,
      password: this.state.password
    };

    this.props.loginUser(userCredentials);
  }

  render() {
    const { errors } = this.state;
    return (
      <div className="bg-gradient-primary" style={{ height: "100vh" }}>
        <div className="container">
          {/* Outer Row */}
          <div className="row justify-content-center">
            <div className="col-xl-10 col-lg-12 col-md-9">
              <div className="card o-hidden border-0 shadow-lg my-5">
                <div className="card-body p-0">
                  {/* Nested Row within Card Body */}
                  <div className="row">
                    <div className="col-lg-6 d-none d-lg-block bg-login-image" />
                    <div className="col-lg-6">
                      <div className="p-5">
                        <div className="text-center">
                          <h1 className="h4 text-gray-900 mb-4">
                            Welcome Back!
                          </h1>
                        </div>
                        <form className="user" onSubmit={this.onSubmit}>
                          <div className="form-group">
                            <TextFieldGroup
                              className="form-control-user"
                              placeholder="Enter Email"
                              name="email"
                              type="email"
                              value={this.state.email}
                              onChange={this.onChange}
                              error={errors.email}
                              info=""
                            />
                          </div>
                          <div className="form-group">
                            <TextFieldGroup
                              className="form-control-user"
                              placeholder="Enter Password"
                              name="password"
                              type="password"
                              value={this.state.password}
                              onChange={this.onChange}
                              error={errors.password}
                              info=""
                            />
                          </div>
                          <div className="form-group">
                            <div className="custom-control custom-checkbox small">
                              <input
                                type="checkbox"
                                className="custom-control-input"
                                id="customCheck"
                              />
                              <label
                                className="custom-control-label"
                                htmlFor="customCheck"
                              >
                                Remember Me
                              </label>
                            </div>
                          </div>
                          <button
                            href="#!"
                            className="btn btn-primary btn-user btn-block"
                            type="submit"
                          >
                            Login
                          </button>
                          <hr />
                          {/* <a
                            href="#!"
                            className="btn btn-google btn-user btn-block"
                          >
                            <i className="fab fa-google fa-fw" /> Login with
                            Google
                          </a>
                          <a
                            href="#!"
                            className="btn btn-facebook btn-user btn-block"
                          >
                            <i className="fab fa-facebook-f fa-fw" /> Login with
                            Facebook
                          </a> */}
                        </form>
                        {/* <hr /> */}
                        {/* <div className="text-center">
                          <a className="small" href="forgot-password.html">
                            Forgot Password?
                          </a>
                        </div>
                        <div className="text-center">
                          <a className="small" href="register.html">
                            Create an Account!
                          </a>
                        </div> */}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

// export default Login;

export default connect(
  mapStateToProps,
  { loginUser }
)(withRouter(Login));
