import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getOrders } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";

class Orders extends Component {
  componentDidMount() {
    this.props.getOrders();
  }

  handleRowClick(product) {
    console.log(product);
    this.props.history.push({
      pathname: "/order",
      // search: '?query=abc',
      state: { product: product }
    });
  }

  render() {
    let { orders, loading } = this.props.order;

    let ordersBody = {
      columns: [
        {
          label: "#",
          field: "id"
          //   sort: "asc",
          //   width: 150,
        },
        {
          label: "Total Items",
          field: "items"
          //   sort: "asc",
          // width: 150
        },
        {
          label: "Ordered On",
          field: "orderedOn"
          //   sort: "asc",
          // width: 270
        },
        // {
        //   label: "Customizable",
        //   field: "customize"
        //   //   sort: "asc",
        //   // width: 270
        // },

        {
          label: "Status",
          field: "status"
        }
      ],
      rows: []
    };

    let i = 0;

    if (!isEmpty(orders)) {
      if (orders.length > 0 && !loading) {
        orders.map(order =>
          ordersBody.rows.push({
            id: ++i,
            items: order.products.length,
            orderedOn: order.date.substring(0, 24),
            // customize: (
            //   <span>
            //     {order.isCustom ? (
            //       <i className="text-info fas fa-check" />
            //     ) : (
            //       <i className="text-danger fas fa-window-close" />
            //     )}
            //   </span>
            // ),
            status: order.order_status,

            clickEvent: this.handleRowClick.bind(this, order.products)
          })
        );
      }
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Orders</strong> Information
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container ">
                <MDBDataTable
                  // striped
                  responsive
                  hover
                  theadColor="#FFFF"
                  tbodyColor="#FFFF"
                  bordered
                  className="text-center"
                  data={ordersBody}
                />
              </div>
            )}
            <div className="card-footer">
              {/* <strong>Card</strong> Footer */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders }
)(Orders);
