import React, { Component } from "react";

import { MDBDataTable } from "mdbreact";
import { connect } from "react-redux";
import { getOrders } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";
// import "@fortawesome/fontawesome-free/css/all.min.css";
// import "/css/sb-admin-2.min.css";
// import "mdbreact/dist/css/mdb.css";

class AssignedJobs extends Component {
  componentDidMount() {
    this.props.getOrders();
  }

  handleRowClick(product) {
    this.props.history.push({
      pathname: "/order",
      // search: '?query=abc',
      state: { product: product }
    });
  }

  render() {
    let ordersBody = {
      columns: [
        {
          label: "#",
          field: "id"
          //   sort: "asc",
          //   width: 150
        },
        {
          label: "Product",
          field: "product"
          //   sort: "asc",
          // width: 150
        },
        {
          label: "Ordered On",
          field: "orderedOn"
          //   sort: "asc",
          // width: 270
        },
        {
          label: "Status",
          field: "status"
        }
      ],
      rows: []
    };

    let { orders, loading } = this.props.order;

    let i = 0;

    if (!isEmpty(orders)) {
      orders = orders.reverse();
      if (orders.length > 0 && !loading) {
        orders = orders.filter(order => order.order_status === "assigned");

        orders.map(order =>
          ordersBody.rows.push({
            id: ++i,
            product: order.products[0]._id.name,
            orderedOn: order.date.substring(0, 24),
            status: order.order_status,
            clickEvent: this.handleRowClick.bind(this, order)
          })
        );
      }
    }
    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Customers</strong> Information
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container">
                <MDBDataTable
                  responsive
                  hover
                  theadColor="blue"
                  tbodyColor="white"
                  bordered
                  className="text-center"
                  data={ordersBody}
                />
              </div>
            )}

            <div className="card-footer" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders }
)(AssignedJobs);
