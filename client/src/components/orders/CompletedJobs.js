import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getOrders } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";
import {
  getVendorBidsOnOrder,
  notifyVendor,
  shipOrderToCustomer
} from "../../actions/orderActions";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";

class CompletedJobs extends Component {
  constructor(props) {
    super(props);
    this.state = {
      order_id: "",
      shipment_id: "",
      shipment_mode: "",
      shipment_details: "",
      shipment_date: ""
    };

    this.onChange = this.onChange.bind(this);
    this.addShipment = this.addShipment.bind(this);
    // this.onResetClick = this.onResetClick.bind(this);
    // this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  onNotifyVendor = (v_id, o_id) => {
    console.log("vendor id ", v_id, " order_id ", o_id);
    this.props.notifyVendor(o_id, v_id, this.props.history);
  };

  addShipment = o_id => {
    this.setState({ order_id: o_id });
  };
  onSubmitShipment = () => {
    const newShipment = {
      shipment_id: this.state.shipment_id,
      shipment_mode: this.state.shipment_mode,
      shipment_details: this.state.shipment_details,
      shipment_date: this.state.shipment_date
    };

    this.props.shipOrderToCustomer(
      this.state.order_id,
      newShipment,
      this.props.history
    );
    // console.log(newShipment);
  };

  componentDidMount() {
    // console.log("kya chutyaap hy");
    this.props.getOrders();
    // this.props.getVendorBidsOnOrder(this.props.order);
    // console.log(window.location.pathname);
  }

  render() {
    let { orders, loading } = this.props.order;

    let ordersTable = null;

    let i = 0;

    if (!isEmpty(orders)) {
      orders = orders.reverse();
      // console.log(orders);
      orders = orders.filter(
        order =>
          order.order_status === "completed" ||
          order.order_status === "delievered"
      );
      // console.log(orders);
      if (orders.length > 0 && !loading) {
        ordersTable = orders.map(order => {
          // console.log(order);
          if (order.order_status === "completed") {
            return (
              <tr key={order._id} className="text-center">
                <td>{++i}</td>
                <td>{order.products[0]._id.name}</td>
                <td>{order.date.substring(0, 24)}</td>
                <td>{order.order_status}</td>
                <td>
                  <Link
                    to="#!"
                    data-toggle="modal"
                    data-target="#userData"
                    // onClick={this.addShipment.bind(this, order._id)}
                    onClick={this.addShipment.bind(this, order._id)}
                    className="fas fa-plus"
                  >
                    {" "}
                    Add
                  </Link>
                </td>
                <td>
                  <button
                    onClick={this.onNotifyVendor.bind(
                      this,
                      order.vendor,
                      order._id
                    )}
                    className="btn btn-primary"
                  >
                    {" "}
                    Notify Vendor
                  </button>
                </td>
              </tr>
            );
          } else if (order.order_status === "delievered") {
            return (
              <tr key={order._id} className="text-center">
                <td>{++i}</td>
                <td>{order.products[0]._id.name}</td>
                <td>{order.date.substring(0, 24)}</td>
                <td>{order.order_status}</td>
                <td>
                  <Link
                    to="#!"
                    data-toggle="modal"
                    data-target="#userData"
                    onClick={this.addShipment.bind(this, order._id)}
                    // onClick={this.addShipment.bind(this, order._id)}
                    className="fas fa-plus"
                  >
                    {" "}
                    Add
                  </Link>
                </td>
                <td>Notified</td>
              </tr>
            );
          } else {
            return null;
          }
        });
      }
    }

    if (ordersTable === null) {
      ordersTable = (
        <tr key="1">
          <td>No Completed Jobs found</td>
          <td />
          <td />
          <td />
          <td />
        </tr>
      );
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Completed</strong> Jobs/Orders
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container table-responsive">
                <div className="form-group  " style={{ marginRight: "10px" }}>
                  <table className="table table-hover">
                    <thead className="thead-dark">
                      <tr className="text-center">
                        <th>#</th>
                        <th>Product</th>
                        <th>Order on</th>
                        <th>Status</th>
                        <th>Shipment</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>{ordersTable}</tbody>
                  </table>
                </div>
                <div className="modal fade" id="userData" role="dialog">
                  <div className="modal-dialog">
                    {/* Modal content*/}
                    <div className="modal-content">
                      <div className="modal-header">
                        Ship this Order
                        <i
                          className="fa fa-window-close"
                          data-dismiss="modal"
                          aria-hidden="true"
                        />
                      </div>
                      <div className="modal-body">
                        {/* <p>Admin Panel User</p> */}

                        <div className="col-md-12">
                          <div className="form-group row">
                            <div className="col-md-6">
                              Shipment Id
                              <TextFieldGroup
                                placeholder="Enter Shipment ID"
                                name="shipment_id"
                                value={this.state.shipment_id}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>

                            <div className="col-md-6">
                              Shipping Method
                              <TextFieldGroup
                                placeholder="Enter Shipping Method"
                                name="shipment_mode"
                                value={this.state.shipment_mode}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>

                            <div className="col-md-6">
                              Shipment Notes
                              <TextAreaFieldGroup
                                placeholder="Shipment Notes"
                                name="shipment_details"
                                value={this.state.shipment_details}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>
                            <div className="col-md-6">
                              Date
                              <TextFieldGroup
                                type="date"
                                name="shipment_date"
                                value={this.state.shipment_date}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>
                          </div>
                          {/* </li> */}
                          {/* </ul> */}
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-sm btn-primary"
                          data-dismiss="modal"
                          onClick={this.onSubmitShipment}
                        >
                          Send
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className="card-footer" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders, getVendorBidsOnOrder, notifyVendor, shipOrderToCustomer }
)(CompletedJobs);
