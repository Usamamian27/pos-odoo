import React, { Component } from "react";
import isEmpty from "../common/is-empty";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import {
  getVendorBidsOnOrder,
  assignOrderToVendor,
  shipOrderToCustomer
} from "../../actions/orderActions";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";

import Carousel from "../common/Carousel";

class Order extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      order: null,
      errors: {},
      order_id: "",
      shipment_id: "",
      shipment_mode: "",
      shipment_details: "",
      shipment_date: ""
    };
  }

  componentDidMount() {
    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      this.props.history.push("/orders");
    } else {
      this.setState({
        order: this.props.location.state.product
      });
      // this.props.getVendorBidsOnOrder(this.props.location.state.product._id);
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  assignOrderToVendor(v_id, o_id) {
    this.props.assignOrderToVendor(o_id, v_id, this.props.history);
  }
  addShipment = o_id => {
    this.setState({ order_id: o_id });
  };
  onSubmitShipment = () => {
    const newShipment = {
      shipment_id: this.state.shipment_id,
      shipment_mode: this.state.shipment_mode,
      shipment_details: this.state.shipment_details,
      shipment_date: this.state.shipment_date
    };

    this.props.shipOrderToCustomer(
      this.state.order_id,
      newShipment,
      this.props.history
    );
    // console.log(newShipment);
  };

  render() {
    const { bids, bidsLoading } = this.props.order;

    let order;

    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      this.props.history.push("/orders");
    } else {
      order = this.props.location.state.product;
    }

    let i = 0;

    let data;
    if (order) {
      data = order.map(
        product => (
          console.log(product),
          (
            <div key={product._id} className="container">
              <div className="card mb-4">
                <div className="card-body">
                  <div className="row justify-content-center">
                    <div className="col-md-8 col-sm-10 col-lg-6 col-xl-4 col-xs-10">
                      <Carousel photoUrl={product.photoUrl} />
                    </div>

                    <div className="col-md-8 col-sm-10 col-lg-6 col-xl-6 col-xs-10">
                      <div>
                        <h3 className="product-title">{product.name}</h3>

                        <p className="product-description">
                          Suspendisse quos? Tempus cras iure temporibus? Eu
                          laudantium cubilia sem sem! Repudiandae et! Massa
                          senectus enim minim sociosqu delectus posuere.
                        </p>

                        <p>
                          Price:{" "}
                          <span className="text-success">
                            ${parseFloat(product.salesPrice).toFixed(2)}
                          </span>
                        </p>
                        <p>
                          Product Type:{" "}
                          <span className="text-success">
                            {product.productType}
                          </span>
                        </p>

                        {/* <p>
                      Category : <strong>{product.category.name}</strong>
                    </p> */}

                        {/* <p>
                        Size :{" "}
                        <strong>
                          {product.productSize ? product.productSize : "M"}
                        </strong>
                      </p> */}
                      </div>
                      <div
                        className="form-group row justify-content-center card-footer m-2 p-2"
                        style={{ float: "right" }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )
        )
      );
    } else {
      data = <h4>Nothing to show </h4>;
    }
    return <div>{data}</div>;
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getVendorBidsOnOrder, assignOrderToVendor, shipOrderToCustomer }
)(withRouter(Order));
