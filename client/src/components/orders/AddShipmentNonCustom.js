import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getOrders, shipOrderToCustomer } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";

class AddShipmentNonCustom extends Component {
  state = {
    order_id: "",
    shipment_id: "",
    shipment_mode: "",
    shipment_details: "",
    shipment_date: ""
  };

  componentDidMount() {
    this.props.getOrders();
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleRowClick(product) {
    console.log(product);
    this.props.history.push({
      pathname: "/order",
      state: { product: product }
    });
  }
  addShipment = o_id => {
    this.setState({ order_id: o_id });
  };
  onSubmitShipment = () => {
    const newShipment = {
      shipment_id: this.state.shipment_id,
      shipment_mode: this.state.shipment_mode,
      shipment_details: this.state.shipment_details,
      shipment_date: this.state.shipment_date
    };

    this.props.shipOrderToCustomer(
      this.state.order_id,
      newShipment,
      this.props.history
    );
    // console.log(newShipment);
  };

  render() {
    let { orders, loading } = this.props.order;

    let ordersBody = {
      columns: [
        {
          label: "#",
          field: "id"
          //   sort: "asc",
          //   width: 150,
        },
        {
          label: "Total Items",
          field: "items"
          //   sort: "asc",
          // width: 150
        },
        {
          label: "Ordered On",
          field: "orderedOn"
          //   sort: "asc",
          // width: 270
        },
        {
          label: "Status",
          field: "status"
        },
        {
          label: "Action",
          field: "action"
        }
      ],
      rows: []
    };

    let i = 0;

    if (!isEmpty(orders)) {
      if (orders.length > 0 && !loading) {
        orders = orders.filter(order => order.isCustom === false);
        console.log(orders);

        orders.map(order =>
          ordersBody.rows.push({
            id: ++i,
            items: order.products.length,
            orderedOn: order.date.substring(0, 24),
            status: "pending",
            action: (
              <Link
                to="#!"
                data-toggle="modal"
                data-target="#userData"
                onClick={this.addShipment.bind(this, order._id)}
                // onClick={this.addShipment.bind(this, order._id)}
                className="fas fa-plus"
              >
                {" "}
                Add
              </Link>
            )
            // clickEvent: this.handleRowClick.bind(this, order)
          })
        );
      }
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Orders</strong> Information
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container ">
                <MDBDataTable
                  // striped
                  responsive
                  hover
                  theadColor="#FFFF"
                  tbodyColor="#FFFF"
                  bordered
                  className="text-center"
                  data={ordersBody}
                />
                <div className="modal fade" id="userData" role="dialog">
                  <div className="modal-dialog">
                    {/* Modal content*/}
                    <div className="modal-content">
                      <div className="modal-header">
                        Ship this Order
                        <i
                          className="fa fa-window-close"
                          data-dismiss="modal"
                          aria-hidden="true"
                        />
                      </div>
                      <div className="modal-body">
                        {/* <p>Admin Panel User</p> */}

                        <div className="col-md-12">
                          <div className="form-group row">
                            <div className="col-md-6">
                              Shipment Id
                              <TextFieldGroup
                                placeholder="Enter Shipment ID"
                                name="shipment_id"
                                value={this.state.shipment_id}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>

                            <div className="col-md-6">
                              Shipping Method
                              <TextFieldGroup
                                placeholder="Enter Shipping Method"
                                name="shipment_mode"
                                value={this.state.shipment_mode}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>

                            <div className="col-md-6">
                              Shipment Notes
                              <TextAreaFieldGroup
                                placeholder="Shipment Notes"
                                name="shipment_details"
                                value={this.state.shipment_details}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>
                            <div className="col-md-6">
                              Date
                              <TextFieldGroup
                                type="date"
                                name="shipment_date"
                                value={this.state.shipment_date}
                                onChange={this.onChange}
                                // error={errors.name}
                                info=""
                              />
                            </div>
                          </div>
                          {/* </li> */}
                          {/* </ul> */}
                        </div>
                      </div>
                      <div className="modal-footer">
                        <button
                          type="button"
                          className="btn btn-sm btn-primary"
                          data-dismiss="modal"
                          onClick={this.onSubmitShipment}
                        >
                          Send
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className="card-footer">
              {/* <strong>Card</strong> Footer */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders, shipOrderToCustomer }
)(AddShipmentNonCustom);
