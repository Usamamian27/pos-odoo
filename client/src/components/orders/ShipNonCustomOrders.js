import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getOrders } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";
import { getVendorBidsOnOrder } from "../../actions/orderActions";
import TextFieldGroup from "../common/TextFieldGroup";
import TextAreaFieldGroup from "../common/TextAreaFieldGroup";

class ShipNonCustomOrders extends Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = {
  //       order_id: "",
  //       shipment_id: "",
  //       shipment_mode: "",
  //       shipment_details: "",
  //       shipment_date: ""
  //     };

  //     this.onChange = this.onChange.bind(this);
  //     this.addShipment = this.addShipment.bind(this);
  //     // this.onResetClick = this.onResetClick.bind(this);
  //     // this.onSubmit = this.onSubmit.bind(this);
  //   }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }
  //   onNotifyVendor = (v_id, o_id) => {
  //     console.log("vendor id ", v_id, " order_id ", o_id);
  //     this.props.notifyVendor(o_id, v_id, this.props.history);
  //   };

  //   addShipment = o_id => {
  //     this.setState({ order_id: o_id });
  //   };
  //   onSubmitShipment = () => {
  //     const newShipment = {
  //       shipment_id: this.state.shipment_id,
  //       shipment_mode: this.state.shipment_mode,
  //       shipment_details: this.state.shipment_details,
  //       shipment_date: this.state.shipment_date
  //     };

  //     this.props.shipOrderToCustomer(
  //       this.state.order_id,
  //       newShipment,
  //       this.props.history
  //     );
  //     // console.log(newShipment);
  //   };

  componentDidMount() {
    // console.log("kya chutyaap hy");
    this.props.getOrders();
    // this.props.getVendorBidsOnOrder(this.props.order);
    // console.log(window.location.pathname);
  }

  render() {
    let { orders, loading } = this.props.order;

    let ordersTable = null;

    let i = 0;

    if (!isEmpty(orders)) {
      // console.log(orders);
      orders = orders.filter(
        order => order.isCustom === false && order.order_status === "Shipped"
      );
      // console.log(orders);
      if (orders.length > 0 && !loading) {
        ordersTable = orders.map(order => {
          // console.log(order);
          if (order.order_status === "Shipped") {
            return (
              <tr key={order._id} className="text-center">
                <td>{++i}</td>
                <td>{order.products[0]._id.name}</td>
                <td>{order.date.substring(0, 24)}</td>
                <td>{order.order_status}</td>
              </tr>
            );
          } else {
            return null;
          }
        });
      }
    }

    if (ordersTable === null) {
      ordersTable = (
        <tr key="1">
          <td>No Completed Jobs found</td>
          <td />
          <td />
          <td />
          <td />
        </tr>
      );
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Shipped</strong> Orders
            </div>
            <br />

            {loading ? (
              <Spinner />
            ) : (
              <div className="container table-responsive">
                <div className="form-group  " style={{ marginRight: "10px" }}>
                  <table className="table table-hover">
                    <thead className="thead-dark">
                      <tr className="text-center">
                        <th>#</th>
                        <th>Product</th>
                        <th>Order on</th>
                        <th>Status</th>
                      </tr>
                    </thead>
                    <tbody>{ordersTable}</tbody>
                  </table>
                </div>
              </div>
            )}
            <div className="card-footer" />
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders }
)(ShipNonCustomOrders);
