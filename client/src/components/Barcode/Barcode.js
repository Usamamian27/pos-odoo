import React, { Component } from "react";
import BarcodeReader from "react-barcode-reader";
import Axios from "axios";

class Test extends Component {
  constructor(props) {
    super(props);
    this.state = {
      result: ""
    };

    this.handleScan = this.handleScan.bind(this);
  }
  handleScan(data) {
    Axios.get(`/api/barcode/search/${data}`)
      .then(res => {
        this.setState({
          result: res.data
        });
        this.props.onSaveDataToState(res.data);
      })
      .catch(err => console.log(err));
  }
  handleError(err) {
    console.error(err);
  }
  render() {
    return (
      <div>
        <BarcodeReader onError={this.handleError} onScan={this.handleScan} />
        {this.state.result === "" || this.state.result === [] ? null : (
          <div className="col-md-12" style={{ display: "flex" }}>
            <img
              src={this.state.result.photoUrl[0]}
              alt=""
              style={{
                marginBottom: 10,
                maxHeight: 250
              }}
            />
            <span className="ml-5">
              <h1>{this.state.result.name}</h1>

              <h1 className="text-success">
                Rs. {this.state.result.salesPrice}
              </h1>
            </span>
          </div>
        )}
      </div>
    );
  }
}

export default Test;
