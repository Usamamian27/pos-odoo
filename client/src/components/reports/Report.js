import React, { Component } from "react";
import { MDBDataTable } from "mdbreact";
// import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { getOrders } from "../../actions/orderActions";
import Spinner from "../common/Spinner";
import isEmpty from "../common/is-empty";
import TextFieldGroup from "../common/TextFieldGroup";

class Report extends Component {
  state = {
    selectedDate: ""
  };
  componentDidMount() {
    this.props.getOrders();
  }

  handleRowClick(product) {
    console.log(product);
    this.props.history.push({
      pathname: "/order",
      // search: '?query=abc',
      state: { product: product }
    });
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
    var yyyy = today.getFullYear();

    today = yyyy + "-" + mm + "-" + dd;
    today = Date(today).substring(0, 16);

    let { orders, loading } = this.props.order;
    let total = 0;

    let ordersBody = {
      columns: [
        {
          label: "Products",
          field: "id"
          //   sort: "asc",
          //   width: 150,
        },
        {
          label: "Quantity",
          field: "items"
          //   sort: "asc",
          // width: 150
        },
        {
          label: "Total",
          field: "totalAmount"
          //   sort: "asc",
          // width: 270
        },
        {
          label: "Date",
          field: "orderedOn"
          //   sort: "asc",
          // width: 270
        }

        // {
        //   label: "Status",
        //   field: "status"
        // }
      ],
      rows: []
    };

    let i = 0;

    if (!isEmpty(orders)) {
      if (orders.length > 0 && !loading) {
        if (this.state.selectedDate == "") {
          orders = orders.filter(item => item.date.substring(0, 16) == today);
        } else {
          let selected = new Date(this.state.selectedDate);
          selected = selected.toString().substring(0, 16);
          orders = orders.filter(
            item => item.date.substring(0, 16) == selected
          );
        }

        orders.map(
          order => (
            order.products.map(pro =>
              ordersBody.rows.push({
                id: pro.name,
                items: pro.productQuantity,
                totalAmount: "$" + pro.salesPrice * pro.productQuantity,
                orderedOn: order.date.substring(0, 24),
                clickEvent: this.handleRowClick.bind(this, order.products)
              })
            ),
            (total += order.totalBill)
          )
        );
      }
    }

    return (
      <div className="row justify-content-center">
        <div className="col-md-10">
          <div className="card">
            <div className="card-header">
              <strong>Sales</strong> Report
              <span style={{ float: "right" }}>
                <strong>
                  Today's Total :{" "}
                  <span className="text-success">Rs. {total}</span>
                </strong>
              </span>
              {/* <strong>
                {" "}
                <span style={{ float: "right" }}>Search here for anything</span>
              </strong> */}
            </div>
            <br />

            <div className="col-md-12 col-lg-12 col-sm-12">
              <TextFieldGroup
                type="date"
                placeholder="Select Date"
                name="selectedDate"
                value={this.state.selectedDate}
                onChange={this.onChange}
                info=""
              />
            </div>

            {loading ? (
              <Spinner />
            ) : (
              <div className="container ">
                <MDBDataTable
                  // striped
                  responsive
                  hover
                  theadColor="#FFFF"
                  tbodyColor="#FFFF"
                  bordered
                  className="text-center"
                  data={ordersBody}
                />
              </div>
            )}
            <div className="card-footer">
              {/* <strong>Card</strong> Footer */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  order: state.order
});

export default connect(
  mapStateToProps,
  { getOrders }
)(Report);
