import React from "react";
import classnames from "classnames";
import PropTypes from "prop-types";

const TextFieldGroup = ({
  className,
  min,
  name,
  placeholder,
  value,
  label,
  error,
  info,
  type,
  onChange,
  disabled
}) => {
  // TextFieldGroup.propTypes = {
  //   name: PropTypes.string.isRequired,
  //   placeholder: PropTypes.string,
  //   value: PropTypes.string.isRequired,
  //   info: PropTypes.string,
  //   error: PropTypes.string,
  //   type: PropTypes.string.isRequired,
  //   onChange: PropTypes.func.isRequired,
  //   disabled: PropTypes.string
  // };

  TextFieldGroup.defaultProps = {
    type: "text"
  };
  return (
    <div className="form-group">
      <input
        type={type}
        className={classnames("form-control " + className, {
          "is-invalid": error
        })}
        placeholder={placeholder}
        name={name}
        value={value}
        min={min}
        onChange={onChange}
        disabled={disabled}
      />
      {info && <small className="form-text text-muted">{info}</small>}
      {error && <div className="invalid-feedback">{error}</div>}

      {/* trying a new version */}
      {/* <div className="form-group row">
        <label className="col-md-3 col-form-label" htmlFor="text-input">
          Name
        </label>
        <div className="col-md-6">
          <input
            className="form-control"
            id="name"
            type="text"
            name="name"
            placeholder="Name of Category"
          />
        </div>
         */}
    </div>
  );
};
export default TextFieldGroup;
