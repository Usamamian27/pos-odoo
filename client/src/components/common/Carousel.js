import React, { Component } from "react";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

class DemoCarousel extends Component {
  render() {
    let { photoUrl } = this.props;
    photoUrl = photoUrl[0].split(",");

    // let p;
    // p.push(item.photoUrl[0].split(","));

    return (
      <Carousel
        infiniteLoop={true}
        showStatus={false}
        autoPlay={true}
        // width={"350px"}
        centerMode={true}
        dynamicHeight={false}
        selectedItem={0}
      >
        {photoUrl.map(url => (
          <div key={url}>
            <img alt="pic" className="img-fluid" src={url} />
          </div>
        ))}
      </Carousel>
    );
  }
}

export default DemoCarousel;
