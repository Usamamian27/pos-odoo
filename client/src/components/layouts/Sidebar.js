import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

class Sidebar extends Component {
  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
  }
  render() {
    return (
      <div>
        <ul
          className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
          id="accordionSidebar"
          style={{ position: "relative", height: "100%" }}
        >
          {/* Sidebar - Brand */}
          <Link
            className="sidebar-brand d-flex align-items-center justify-content-center"
            to="/"
          >
            <div className="sidebar-brand-icon rotate-n-15">
              <i className="fas fa-laugh-wink" />
            </div>
            <div className="sidebar-brand-text mx-3">
              American Bakery
              {/* <sup>2</sup> */}
            </div>
          </Link>

          {/* Divider */}
          <hr className="sidebar-divider my-0" />
          {/* Nav Item - Dashboard */}
          <li className="nav-item active">
            <Link className="nav-link" to="/">
              <i className="fas fa-fw fa-tachometer-alt" />
              <span>Dashboard</span>
            </Link>
          </li>
          {/* Divider */}
          <hr className="sidebar-divider" />
          {/* Heading */}
          <div className="sidebar-heading">Interface</div>

          <li className="nav-item">
            <Link className="nav-link collapsed" to="/sell">
              <i className="fas fa-shopping-cart" />
              <span>Sell</span>
            </Link>
          </li>

          {/* Nav Item - Order Collapse Menu */}
          <li className="nav-item">
            <a
              className="nav-link collapsed"
              href="#!"
              data-toggle="collapse"
              data-target="#orderUtilities"
              aria-expanded="true"
              aria-controls="collapseUtilities"
            >
              <i className="fas fa-shopping-bag" />
              <span>Order Management</span>
            </a>
            <div
              id="orderUtilities"
              className="collapse"
              aria-labelledby="headingUtilities"
              data-parent="#accordionSidebar"
            >
              <div className="bg-white py-2 collapse-inner rounded">
                <Link className="collapse-item" to="/orders">
                  <i className="fas fa-eye" /> New Orders
                </Link>
              </div>
            </div>
          </li>

          {/* Nav Item - Products Collapse Menu */}
          <li className="nav-item">
            <a
              className="nav-link collapsed"
              href="#!"
              data-toggle="collapse"
              data-target="#collapseTwo"
              aria-expanded="true"
              aria-controls="collapseTwo"
            >
              <i className="fab fa-product-hunt" />
              <span>Products</span>
            </a>
            <div
              id="collapseTwo"
              className="collapse"
              aria-labelledby="headingTwo"
              data-parent="#accordionSidebar"
            >
              <div className="bg-white py-2 collapse-inner rounded">
                {/* <h6 className="collapse-header">Custom Components:</h6> */}
                <Link className="collapse-item" to="/add-product">
                  <i className="fas fa-plus" /> Add Product
                </Link>
                <Link className="collapse-item" to="/products">
                  <i className="fas fa-eye" /> View Products
                </Link>
              </div>
            </div>
          </li>

          <br />

          {/* Divider */}
          <hr className="sidebar-divider" />
          {/* Heading */}
          <li className="nav-item">
            <a
              className="nav-link collapsed"
              href="#!"
              data-toggle="collapse"
              data-target="#collapsePages"
              aria-expanded="true"
              aria-controls="collapsePages"
            >
              <i className="fas fa-fw fa-folder" />
              <span>Reports</span>
            </a>
            <div
              id="collapsePages"
              className="collapse"
              aria-labelledby="headingPages"
              data-parent="#accordionSidebar"
            >
              <div className="bg-white py-2 collapse-inner rounded">
                <Link className="collapse-item" to="/daily">
                  View Report
                </Link>
                {/* <Link className="collapse-item" to="/monthly">
                  Monthly
                </Link> */}
              </div>
            </div>
          </li>

          <br />

          {/* Divider */}
          <hr className="sidebar-divider d-none d-md-block" />
          {/* Sidebar Toggler (Sidebar) */}
          <div className="text-center d-none d-md-inline">
            <button className="rounded-circle border-0" id="sidebarToggle" />
          </div>
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(mapStateToProps)(Sidebar);
