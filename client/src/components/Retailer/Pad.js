import React, { Component } from "react";
// import NumPad from "react-numpad";
import classnames from "classnames";
import { connect } from "react-redux";
import { PlaceOrder } from "../../actions/orderActions";
import { withRouter } from "react-router-dom";
import "./Pad.css";
class Pad extends Component {
  state = {
    total: "",
    duePositiveAmount: 0,
    tendered: "",
    change: 0,
    balance: 0,
    negativeStyle: false,
    duePositiveUI: "",
    products: null
  };
  componentDidMount() {
    const { subTotal, cart } = this.props.location.state;

    if (subTotal === 0) {
      this.props.history.push("/sell");
    } else {
      this.setState({ total: subTotal, products: cart });
    }
  }

  onValueChanged = e => {
    let balance = 0;

    this.setState({ tendered: this.state.tendered + "" + e }, () => {
      this.setState({ balance: this.state.total - this.state.tendered }, () => {
        if (this.state.balance < 0) {
          balance = Math.abs(this.state.balance);
          this.setState({ negativeStyle: true });
          this.setState({ change: balance, duePositiveAmount: 0 });
        } else {
          let duePositiveUI = "";
          this.setState({
            duePositiveAmount: balance,
            change: 0,
            negativeStyle: false
          });
          duePositiveUI = (
            <tr>
              <td style={{ border: "2px solid black" }}>
                {this.state.duePositiveAmount}
              </td>
              <td />
              <td />
              <td />
            </tr>
          );
          this.setState({ duePositiveUI: duePositiveUI });
        }
      });
    });
  };

  onValueChanged2 = e => {
    let balance = 0;
    this.setState({ [e.target.name]: e.target.value }, () => {
      this.setState({ balance: this.state.total - this.state.tendered }, () => {
        if (this.state.balance < 0) {
          console.log("negative ");
          balance = Math.abs(this.state.balance);
          this.setState({ negativeStyle: true });
          this.setState({ change: balance, duePositiveAmount: 0 });
        } else {
          let duePositiveUI = "";
          this.setState({
            duePositiveAmount: balance,
            change: 0,
            negativeStyle: false
          });
          duePositiveUI = (
            <tr>
              <td style={{ border: "2px solid black" }}>
                {this.state.duePositiveAmount}
              </td>
              <td />
              <td />
              <td />
            </tr>
          );
          this.setState({ duePositiveUI: duePositiveUI });
        }
      });
    });
  };
  onclear = e => {
    this.setState({
      tendered: 0,
      change: 0
    });
  };
  onClearCal = () => {
    this.setState({
      tendered: "",
      change: 0
    });
  };
  onSubmitOrder = e => {
    e.preventDefault();
    console.log("cart", this.state.products);

    let newOrder = {
      products: this.state.products,
      totalBill: this.state.total,
      amountPaid: this.state.tendered
    };
    if (
      this.state.tendered > this.state.total ||
      this.state.tendered == this.state.total
    ) {
      this.props.PlaceOrder(newOrder, this.props.history);
    } else {
      alert("Amount Paid is less than total");
    }
  };
  render() {
    const { subTotal } = this.props.location.state;
    if (subTotal === 0) {
      this.props.history.push("/sell");
    } else {
      console.log("subTotal : ", subTotal);
    }
    return (
      <div className="container">
        <div className="payment-screen screen">
          <div className="screen-content">
            <div style={{ display: "flex", justifyContent: "space-between" }}>
              <div>
                <h1>Payment</h1>
              </div>

              <div>
                <button
                  className="btn btn-lg  btn-outline-dark"
                  type="button"
                  disabled={
                    this.state.tendered === 0 ||
                    this.state.tendered < this.state.total
                      ? true
                      : false
                  }
                  onClick={this.onSubmitOrder}
                >
                  Complete Order
                </button>
              </div>
            </div>

            {/* <div className="left-content pc40 touch-scrollable scrollable-y">
              <div className="paymentmethods-container">
                <div className="paymentmethods">
                  <div className="button paymentmethod" data-id={8}>
                    Cash (USD)
                  </div>
                </div>
              </div>
            </div> */}
            <div className=" touch-scrollable scrollable-y">
              <section className="container">
                <table className=" table table-stripped">
                  <thead className="thead-dark">
                    <tr className="label">
                      <th>Due</th>
                      <th>Tendered</th>
                      <th>Change</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr className="paymentline selected">
                      <td className="positive-change"> ${subTotal} </td>
                      <td
                        className=""
                        style={{
                          border: "3px solid #6EC89B",
                          backgroundColor: "white",
                          color: "#6EC89B",
                          padding: 0,
                          width: "33%"
                        }}
                      >
                        <form onSubmit={this.onSubmitOrder}>
                          <input
                            autoFocus
                            type="number"
                            value={this.state.tendered}
                            name="tendered"
                            onChange={this.onValueChanged2}
                            style={{
                              borderRadius: 10,
                              padding: 11,
                              width: "100%",
                              border: "none",
                              margin: 0
                            }}
                          />
                        </form>
                      </td>
                      <td
                        className={classnames({
                          "edit positive-change ":
                            this.state.negativeStyle === false,

                          "negative-change": this.state.negativeStyle === true
                        })}
                        // className={classnames(
                        //   "col-tendered edit positive-change ",
                        //   {
                        //     "negative-change": this.state.negativeStyle === true
                        //   }
                        // )}
                        // style={{ backgroundColor: "#6EC89B", color: "white" }}
                      >
                        ${this.state.change == 0 ? "0.00" : this.state.change}
                      </td>
                    </tr>
                  </tbody>
                </table>
              </section>

              {/* <button
                className="btn btn-lg  btn-outline-primary"
                type="button"
                // disabled={total === 0 ? true : false}
                onClick={this.onSubmitOrder}
              >
                Complete Order
              </button> */}
            </div>
          </div>
        </div>
        <div className="row justify-content-center text-center">
          <table
            cellPadding={2}
            cellSpacing={2}
            border={0}
            style={{ backgroundColor: "lightgrey" }}
            id="keyboard"
          >
            <tbody>
              <tr>
                <td style={{ borderRadius: 20 }}>
                  <input
                    type="button"
                    onChange={this.onValueChanged}
                    defaultValue={1}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(1)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={2}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(2)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={3}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(3)}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={4}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(4)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={5}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(5)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={6}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(6)}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={7}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(7)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={8}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(8)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={9}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(9)}
                  />
                </td>
              </tr>
              <tr>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue="."
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(".")}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue={0}
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={() => this.onValueChanged(0)}
                  />
                </td>
                <td>
                  <input
                    type="Button"
                    onChange={this.onValueChanged}
                    defaultValue="  C  "
                    style={{
                      fontSize: 25,
                      borderColor: "transparent",
                      backgroundColor: "white"
                    }}
                    onClick={this.onClearCal}
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { PlaceOrder }
)(withRouter(Pad));
