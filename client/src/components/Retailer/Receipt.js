import React, { Component } from "react";
import { connect } from "react-redux";
import { setDataToState } from "../../actions/productActions";

class Receipt extends Component {
  onNewOrderClicked = () => {
    let emptyArray = [];
    localStorage.removeItem("state");
    this.props.setDataToState(emptyArray);
    this.props.history.replace("/sell");
  };

  render() {
    let i = 0;
    const { order } = this.props.location.state;

    let purchasedItems = null;
    if (order) {
      purchasedItems = order.products.map(product => (
        <tr key={product._id}>
          <th scope="row">{++i}</th>
          <td>{product.name}</td>
          <td>{product.productQuantity}</td>
          <td>{product.salesPrice}</td>
        </tr>
      ));
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12 col-lg-12 col-sm-12">
            <div className="card">
              <div className="card-header">
                <p
                  style={{
                    display: "inline-block",
                    marginLeft: 10,
                    marginTop: 7,
                    position: "absolute"
                  }}
                >
                  Print this Receipt{" "}
                  <i className="fa fa-print" aria-hidden="true" />
                </p>

                <button
                  className="btn btn-dark"
                  style={{ float: "right" }}
                  type="button"
                  onClick={this.onNewOrderClicked}
                >
                  New Order >>>
                </button>
              </div>
              <div className="card-body">
                <h5 className="card-title text-center">Order # {order._id}</h5>
                {/* <div className="card-text">{purchasedItems}</div> */}
                <table className="table" style={{ textAlign: "center" }}>
                  <thead className="thead-dark">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Products</th>
                      <th scope="col">Quantity</th>
                      <th scope="col">Price</th>
                    </tr>
                  </thead>
                  <tbody>{purchasedItems}</tbody>
                </table>

                <div style={{ float: "right", marginRight: 20 }}>
                  <hr />
                  <p>
                    <strong>Total :</strong> Rs. {order.totalBill}
                  </p>
                  <hr />
                  <p>
                    <strong>Cash Rec :</strong> Rs. {order.amountPaid}
                  </p>
                  <p>
                    <strong>Change :</strong> Rs.
                    {Math.abs(order.totalBill - order.amountPaid)}
                  </p>
                </div>
              </div>
              <div className="card-footer text-muted">{Date(Date.now())}</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { setDataToState }
)(Receipt);
