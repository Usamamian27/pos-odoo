import React, { Component } from "react";
// import { Link } from "react-router-dom";
import {
  getProducts,
  setDataToState,
  searchProduct
} from "../../actions/productActions";
import { connect } from "react-redux";
import Barcode from "../Barcode/Barcode";
class Sell extends Component {
  state = {
    searchBar: ""
  };

  componentDidMount() {
    this.props.getProducts();
    let localStorageCart;

    if (JSON.parse(localStorage.getItem("state"))) {
      localStorageCart = JSON.parse(localStorage.getItem("state"));
      this.props.setDataToState(localStorageCart.product.cart);
    } else {
      localStorageCart = [];
    }
  }
  onChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  onSumbitSearch = e => {
    e.preventDefault();
    this.props.searchProduct(this.state.searchBar);
  };
  SaveDataToState(product, productQuantity) {
    let { cart } = this.props.product;

    if (cart.length > 0) {
      let have = false;
      cart = cart.filter(pro => {
        if (pro._id === product._id) {
          have = true;
          if (productQuantity > 0) {
            pro.productQuantity++;
          } else {
            // if (pro.productQuantity !== 0) {
            //   pro.productQuantity--;
            // } else {
            //   this.onDeleteClicked(pro._id);
            // }
            pro.productQuantity--;
          }
        }
        return pro;
      });
      if (!have) {
        product.productQuantity = productQuantity;
        cart.push(product);
      }

      this.props.setDataToState(cart);
    } else {
      product.productQuantity = productQuantity;
      cart.push(product);
      this.props.setDataToState(cart);
    }
  }
  onDeleteClicked(id, ab) {
    let a;
    if (JSON.parse(localStorage.getItem("state"))) {
      a = JSON.parse(localStorage.getItem("state"));
      const result = a.product.cart.filter(item => item._id.toString() !== id);
      a.product.cart = result;
      localStorage.setItem("state", JSON.stringify(a));
      this.props.setDataToState(a.product.cart);
    }
  }
  onIncrementQuantity = item => {
    this.SaveDataToState(item, 1);
  };
  onDecrementQuantity = item => {
    this.SaveDataToState(item, -1);
  };
  onSubmit = (total, cart) => {
    this.props.history.push({
      pathname: "/pad",
      state: { subTotal: total, cart: cart }
    });
  };
  render() {
    let { products, loading, cart } = this.props.product;
    let items;
    let total = 0;
    let cartItems = null;

    if (cart === null || cart === undefined) {
    } else {
      if (cart.length > 0) {
        cart = cart.filter(product => product.productQuantity != 0);
        cartItems = cart.map(
          item => (
            (total = total + item.productQuantity * item.salesPrice),
            (
              <div
                key={item._id}
                className="card"
                style={{ width: "90%", margin: "auto", marginBottom: 4 }}
              >
                <div className="card-header">
                  <div>
                    {item.name}
                    <span style={{ float: "right", fontSize: 15 }}>
                      Rs. {(item.salesPrice * item.productQuantity).toFixed(2)}
                    </span>
                  </div>
                </div>
                {/* <div className="card-header">Featured</div> */}
                <div className="card-body">
                  {/* <h5 className="card-title">Special title treatment</h5> */}
                  <div className="row">
                    {/* <img
                      src={item.photoUrl[0]}
                      style={{ width: 100, height: 100 }}
                    /> */}
                    {/* <h6 className="card-text "></h6> */}
                  </div>
                  {/* <a href="#" className="btn btn-dark">
                    ${item.salesPrice}
                  </a>{" "}
                  X{" "}
                  <a href="#" className="btn btn-dark">
                    {item.productQuantity}
                  </a>{" "}
                  =
                  <a href="#" className="btn btn-dark">
                    ${item.salesPrice * item.productQuantity}
                  </a> */}
                  <h6>
                    <span>
                      <strong>Qty :</strong>{" "}
                    </span>
                    {item.productQuantity}Unit(s) at Rs.{item.salesPrice}
                  </h6>
                </div>
                <div className="card-footer text-muted">
                  <i
                    onClick={() => this.onIncrementQuantity(item)}
                    style={{ fontSize: 20 }}
                    className="fas fa-plus-circle"
                  />
                  {"                              "}
                  {"                           "}
                  <i
                    onClick={() => this.onDecrementQuantity(item)}
                    style={{ fontSize: 20, float: "right" }}
                    className="fas fa-minus-circle"
                  />
                </div>
              </div>
            )
          )
        );
      } else {
        cartItems = "Loading ...";
      }
    }

    if (products === null || loading) {
      // items = <h4>No products Found...</h4>;
    } else {
      if (products.length > 0) {
        items = products.map(item => (
          //   <ProductItem key={item._id} item={item} />
          <div key={item._id} className="col-md-3">
            <div className="card-deck">
              <div className="card mb-4 text-center">
                <div className="card-header">
                  <p className="card-title">
                    <strong>{item.name}</strong>
                  </p>
                </div>
                <img
                  className="card-img-top img-fluid"
                  style={{ width: "100%", height: 100, padding: 5 }}
                  src={
                    item.photoUrl[0]
                      ? item.photoUrl[0].split(",")[0]
                      : "https://cdn.lynda.com/course/713378/713378-636576595892468047-16x9.jpg"
                  }
                  alt="Card cap"
                />

                <div className="card-body">
                  {/* <p className="card-text">
                    <small className="text-muted">
                      Last updated 3 mins ago
                    </small>
                  </p> */}

                  <p className="card-text">
                    <strong className="text-success">
                      Rs. {item.salesPrice}
                    </strong>
                  </p>
                  <button
                    type="button"
                    className="btn btn-dark "
                    onClick={this.SaveDataToState.bind(this, item, 1)}
                  >
                    <i className="fa fa-plus" aria-hidden="true" /> Add
                  </button>
                </div>
              </div>
            </div>
          </div>
        ));
      } else {
        items = <h4>No products Found...</h4>;
      }
    }

    return (
      <div className="container">
        <Barcode onSaveDataToState={item => this.SaveDataToState(item, 1)} />
        <div className="row">
          <div className="col-md-9 col-lg-9 col-sm-12 col-xs-12">
            <div className="out-div">
              <form className="inpt" onSubmit={this.onSumbitSearch}>
                <input
                  type="text"
                  style={{ width: "100%", height: "100%" }}
                  placeholder="Search for..."
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                  name="searchBar"
                  value={this.state.searchBar}
                  onChange={this.onChange}
                />
              </form>
              <div className="icon1">
                <button
                  style={{ width: "100%", height: "100%" }}
                  className="btn btn-dark"
                  type="submit"
                  // onClick={this.onSumbitSearch}
                >
                  <i className="fas fa-search fa-sm" />
                </button>
              </div>
            </div>
            {/* <form
              onSubmit={this.onSumbitSearch}
              style={{
                border: "2px solid gray",
                borderTopRightRadius: 7,
                borderBottomRightRadius: 7,
                marginBottom: 20,
                width: "100%"
              }}
              className=" d-lg-inline-block form-inline  navbar-search text-center"
            >
              <div className="input-group" style={{}}>
                <input
                  type="text"
                  className="form-control bg-light border-0 small"
                  placeholder="Search for..."
                  aria-label="Search"
                  aria-describedby="basic-addon2"
                  name="searchBar"
                  value={this.state.searchBar}
                  onChange={this.onChange}
                />
                <div className="input-group-append">
                  <button
                    className="btn btn-dark"
                    type="submit"
                    // onClick={this.onSumbitSearch}
                  >
                    <i className="fas fa-search fa-sm" />
                  </button>
                </div>
              </div>
            </form> */}

            <div className="row">{items}</div>
          </div>
          <div className="col-md-3 col-lg-3 col-sm-12 colo-xs-12">
            {/* sidebar */}

            <div>
              <div className="nav-side-menu">
                <div className="brand">
                  Selected Items{"      "}
                  <span>&nbsp;&nbsp;</span>
                  <button
                    type="button"
                    className="btn btn-light btn-sm"
                    style={{ borderRadius: 30 }}
                  >
                    <span
                      style={{ borderRadius: 30 }}
                      className="badge badge-dark"
                    >
                      {cartItems == null || cartItems == "Loading ..."
                        ? "0"
                        : cartItems.length}
                    </span>
                  </button>
                </div>
                <div
                  style={{
                    overflow: "auto",
                    maxHeight: "615px"
                  }}
                >
                  {cartItems}
                </div>
                <hr style={{ backgroundColor: "white" }} />
                <div className="text-center" style={{ alignItems: "center" }}>
                  <span
                  // style={{ float: "right" }}
                  >
                    <strong style={{ color: "white", fontSize: 20 }}>
                      Total : Rs. {total.toFixed(2)}
                    </strong>
                  </span>
                </div>
                <div className="text-center" style={{ alignItems: "center" }}>
                  <button
                    autoFocus
                    onClick={() => this.onSubmit(total, cart)}
                    disabled={total === 0 ? true : false}
                    style={{
                      marginTop: 30,
                      //marginLeft: 50,
                      // marginRight: 50,
                      height: 150,
                      padding: 40,
                      width: 200
                    }}
                  >
                    <span>
                      <i
                        className="fas fa-chevron-circle-right"
                        style={{ fontSize: "-webkit-xxx-large" }}
                      />
                    </span>
                    <p style={{ fontSize: 18 }}>Payment</p>
                  </button>
                  {/* <Link
                    style={{ pointerEvents: "none" }}
                    to={{
                      pathname: "/pad",
                      state: { subTotal: total, cart: cart }
                    }}
                    //disabled={total === 0 ? true : false}
                    className="btn btn-lg btn-light"
                    style={{
                      marginTop: 30,
                      //marginLeft: 50,
                      // marginRight: 50,
                      height: 150,
                      padding: 40,
                      width: 200
                    }}
                  >
                    <span>
                      <i
                        className="fas fa-chevron-circle-right"
                        style={{ fontSize: "-webkit-xxx-large" }}
                      />
                    </span>
                    Payment{" "}
                  </Link> */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product
});

export default connect(
  mapStateToProps,
  { getProducts, setDataToState, searchProduct }
)(Sell);
