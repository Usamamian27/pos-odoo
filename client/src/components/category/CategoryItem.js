import React, { Component } from "react";

import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { deleteCategory } from "../../actions/categoryActions";

class CategoryItem extends Component {
  onDeleteClick(id) {
    this.props.deleteCategory(id);
  }

  render() {
    const { item } = this.props;

    return (
      <div className="col-md-3">
        <div className="card-deck">
          <div className="card mb-4">
            <img
              className="card-img-top img-fluid"
              style={{ width: "100%", height: 300 }}
              src={
                item.photoUrl[0]
                  ? item.photoUrl[0].split(",")[0]
                  : "https://cdn.lynda.com/course/713378/713378-636576595892468047-16x9.jpg"
              }
              alt="Card cap"
            />

            <div className="card-body">
              <h4 className="card-title">{item.name}</h4>
              {/* <p className="card-text">{item.subCategory[0].subName}</p> */}
              <p className="card-text">
                <small className="text-muted">Last updated 3 mins ago</small>
              </p>
              <button
                type="button"
                className="btn btn-danger"
                onClick={this.onDeleteClick.bind(this, item._id)}
              >
                <i className="fa fa-trash" aria-hidden="true" /> Delete
              </button>
              <Link
                to={{ pathname: "/edit-category", state: { item: item } }}
                // {{to="/edit-category",state: { foo: 'bar'}}}
                className="btn btn-primary a-btn-slide-text"
                style={{ marginLeft: 4 }}
                params={item}
              >
                <i className="far fa-edit" /> Edit
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  null,
  { deleteCategory }
)(CategoryItem);
