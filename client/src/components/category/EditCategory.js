import React, { Component } from "react";

import TextFieldGroup from "../common/TextFieldGroup";
import SelectListGroup from "../common/SelectListGroup";

import { connect } from "react-redux";
import { updateCategory } from "../../actions/categoryActions";

class EditCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      // subCategory: "",
      photoUrl: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentDidMount() {
    if (
      this.props.location.state === null ||
      this.props.location.state === undefined
    ) {
      this.props.history.push("/categories");
    } else {
      const { item } = this.props.location.state;

      this.setState({
        id: item._id,
        name: item.name
      });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const newCategory = {
      name: this.state.name,
      // subCategory: this.state.subCategory,
      photoUrl: this.state.photoUrl
    };

    // console.log(this.state.subCategory);
    this.props.updateCategory(this.state.id, newCategory, this.props.history);
  }
  render() {
    const { errors } = this.state;

    // Select options
    const options = [
      { label: "Select Sub Category", value: 0 },
      { label: "Custom Suits", value: "Custom Suits" },
      { label: "Blazers", value: "Blazers" },
      { label: "Pants", value: "Pants" }
    ];

    return (
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">
              <strong>Edit</strong> Category
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={this.onSubmit}
                encType="multipart/form-data"
                style={{ marginLeft: "30px" }}
              >
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Name
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Category Name"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      error={errors.name}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Upload Image
                  </label>
                  <div className="col-md-6">
                    <input
                      type="file"
                      name="myFiles"
                      multiple
                      onChange={this.onChange}
                    />
                  </div>
                </div>

                <div
                  className="form-group row float-right card-footer"
                  style={{ marginRight: "10px" }}
                >
                  <button className="btn btn-sm btn-primary" type="submit">
                    <i className="fa fa-dot-circle-o" /> Update Category
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(
  null,
  { updateCategory }
)(EditCategory);
