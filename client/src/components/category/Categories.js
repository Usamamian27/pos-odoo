import React, { Component } from "react";

// import { Link } from "react-router-dom";
import { getCategories } from "../../actions/categoryActions";
import { connect } from "react-redux";
import CategoryItem from "./CategoryItem";

class Categories extends Component {
  componentDidMount() {
    this.props.getCategories();
  }

  render() {
    const { categories, loading } = this.props.category;
    let items;

    if (categories === null || loading) {
      // items = <h4>No Categories Found...</h4>;
    } else {
      if (categories.length > 0) {
        items = categories.map(item => (
          <CategoryItem key={item._id} item={item} />
        ));
      } else {
        items = <h4>No Categories Found...</h4>;
      }
    }

    return (
      <div className="container">
        <div className="row">{items}</div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  category: state.category
});

export default connect(
  mapStateToProps,
  { getCategories }
)(Categories);
