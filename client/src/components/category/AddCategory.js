import React, { Component } from "react";

import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import TextFieldGroup from "../common/TextFieldGroup";
// import SelectListGroup from "../common/SelectListGroup";
import { addCategory } from "../../actions/categoryActions";

import axios from "axios";

class AddCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      // subName: "",
      file: [],
      photoUrl: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onResetClick = this.onResetClick.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  setImages = e => {
    let file = [];
    for (let i = 0; i < e.target.files.length; i++) {
      file.push(e.target.files[i]);
    }

    this.setState({
      file: file
    });
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.errors.errors) {
      this.setState({
        errors: nextProps.errors.errors
      });
    } else {
      this.setState({ errors: "" });
    }
  }

  onSubmit(e) {
    e.preventDefault();

    const newCategory = {
      name: this.state.name,
      // subName: this.state.subName,
      photoUrl: this.state.photoUrl
    };

    let myFiles = this.state.file;

    if (myFiles.length > 0) {
      const fd = new FormData();

      for (let i = 0; i < myFiles.length; i++) {
        fd.append("myFiles", myFiles[i], myFiles[i].name);
      }

      // console.log(...fd);

      axios.post("/api/uploadmultiple", fd).then(res => {
        let url = "";

        for (let i = 0; i < res.data.length; i++) {
          url += res.data[i] + ",";
        }

        url = url.substring(0, url.length - 1);

        newCategory.photoUrl = url;

        this.props.addCategory(newCategory, this.props.history);
      });
    } else {
      alert("Please select atleast one image");
    }
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onResetClick() {
    this.setState({
      name: "",
      // subName: "",
      file: [],
      photoUrl: "",
      errors: {}
    });
  }

  render() {
    const { errors } = this.state;
    // let { images, file } = this.state;

    return (
      <div className="row justify-content-center">
        <div className="col-md-8">
          <div className="card">
            <div className="card-header">
              <strong>Add</strong> Category
            </div>
            <div className="card-body">
              <form
                className="form-horizontal"
                onSubmit={this.onSubmit}
                encType="multipart/form-data"
                style={{ marginLeft: "30px" }}
              >
                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Name
                  </label>
                  <div className="col-md-6">
                    <TextFieldGroup
                      placeholder="Category Name"
                      name="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      error={errors.name}
                      info=""
                    />
                  </div>
                </div>

                <div className="form-group row">
                  <label
                    className="col-md-3 col-form-label"
                    htmlFor="text-input"
                  >
                    Choose Images
                  </label>
                  <div className="col-md-6">
                    <input
                      id="image"
                      name="myFiles"
                      type="file"
                      multiple
                      onChange={this.setImages}
                    />
                  </div>
                </div>

                <div
                  className="form-group row justify-content-center card-footer"
                  style={{ marginRight: "10px" }}
                >
                  <button className="btn btn-sm btn-primary " type="submit">
                    <i className="fa fa-dot-circle-o" /> Add Category
                  </button>
                  <button
                    className="btn btn-sm btn-danger"
                    style={{ marginLeft: 4 }}
                    onClick={this.onResetClick}
                    type="reset"
                  >
                    <i className="fa fa-ban" /> Reset
                  </button>
                </div>
              </form>

              {/* {file.map((item, index) => (
                <img src={item} key={index} />
              ))} */}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  errors: state.errors,
  photoUrl: state.category.photoUrl
});

export default connect(
  mapStateToProps,
  { addCategory }
)(withRouter(AddCategory));
