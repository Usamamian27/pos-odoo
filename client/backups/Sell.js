import React, { Component } from "react";
import { Link } from "react-router-dom";
import { getProducts, setDataToState } from "../../actions/productActions";
import { connect } from "react-redux";
import Pad from "./Pad";
import NumPad from "react-numpad";

class Sell extends Component {
  componentDidMount() {
    this.props.getProducts();
    let localStorageCart;

    if (JSON.parse(localStorage.getItem("state"))) {
      localStorageCart = JSON.parse(localStorage.getItem("state"));
      this.props.setDataToState(localStorageCart.product.cart);
    } else {
      localStorageCart = [];
    }
  }
  SaveDataToState(product, productQuantity) {
    console.log("product", product);

    let { cart } = this.props.product;
    console.log("cart", cart);

    if (cart.length > 0) {
      let have = false;
      cart = cart.filter(pro => {
        if (pro._id === product._id) {
          have = true;

          pro.productQuantity++;
        }
        return pro;
      });
      if (!have) {
        // product.productSize = productSize;
        product.productQuantity = productQuantity;
        cart.push(product);
      }

      this.props.setDataToState(cart);
    } else {
      //   product.productSize = productSize;
      product.productQuantity = productQuantity;
      cart.push(product);
      this.props.setDataToState(cart);
    }
  }

  render() {
    const { products, loading, cart } = this.props.product;
    let items;
    let total = 0;
    let cartItems = null;

    if (cart === null || cart === undefined) {
    } else {
      if (cart.length > 0) {
        cartItems = cart.map(
          item => (
            (total = total + item.productQuantity * item.salesPrice),
            (
              <div key={item._id} className="card">
                {/* <div className="card-header">Featured</div> */}
                <div className="card-body">
                  {/* <h5 className="card-title">Special title treatment</h5> */}
                  <div className="row">
                    <img
                      src={item.photoUrl[0]}
                      style={{ width: 100, height: 100 }}
                    />
                    <p className="card-text">{item.name}</p>
                  </div>
                  <a href="#" className="btn btn-primary">
                    ${item.salesPrice}
                  </a>{" "}
                  X{" "}
                  <a href="#" className="btn btn-primary">
                    {item.productQuantity}
                  </a>{" "}
                  =
                  <a href="#" className="btn btn-primary">
                    ${item.salesPrice * item.productQuantity}
                  </a>
                </div>
              </div>
            )
          )
        );
      } else {
        cartItems = "Loading ...";
      }
    }

    if (products === null || loading) {
      // items = <h4>No products Found...</h4>;
    } else {
      if (products.length > 0) {
        items = products.map(item => (
          //   <ProductItem key={item._id} item={item} />
          <div key={item._id} className="col-md-3">
            <div className="card-deck">
              <div className="card mb-4">
                <img
                  className="card-img-top img-fluid"
                  style={{ width: "100%", height: 200 }}
                  src={
                    item.photoUrl[0]
                      ? item.photoUrl[0].split(",")[0]
                      : "https://cdn.lynda.com/course/713378/713378-636576595892468047-16x9.jpg"
                  }
                  alt="Card cap"
                />

                <div className="card-body">
                  <h4 className="card-title">{item.name}</h4>
                  {/* <p className="card-text">{item.subCategory[0].subName}</p> */}
                  <p className="card-text">
                    <small className="text-muted">
                      Last updated 3 mins ago
                    </small>
                  </p>

                  <p className="card-text">
                    <strong className="text-success">${item.salesPrice}</strong>
                  </p>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={this.SaveDataToState.bind(this, item, 1)}
                  >
                    <i className="fa fa-plus" aria-hidden="true" /> Add
                  </button>
                  {/* <Link
                to={{ pathname: "/edit-category", state: { item: item } }}
                // {{to="/edit-category",state: { foo: 'bar'}}}
                className="btn btn-primary a-btn-slide-text"
                style={{ marginLeft: 4 }}
                params={item}
              >
                <i className="far fa-edit" /> Edit
              </Link> */}
                </div>
              </div>
            </div>
          </div>
        ));
      } else {
        items = <h4>No products Found...</h4>;
      }
    }

    return (
      <div className="container">
        <div className="row">
          <div className="col-md-9 col-lg-9 col-sm-12">
            <div className="row">{items}</div>
          </div>
          <div className="col-md-3 col-lg-3 col-sm-12">
            {/* sidebar */}

            <ul
              className="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion"
              id="accordionSidebar"
              style={{
                backgroundImage: "linear-gradient(#dc3545 10%, #33aedd 100%)",
                backgroundSize: "cover",
                padding: 5
              }}
            >
              <div className="sidebar-brand-text mx-3">
                <strong style={{ color: "white" }}>Selected items</strong>
                {/* <sup>2</sup> */}
              </div>
              {/* Divider */}
              <hr className="sidebar-divider my-0 mb-1" />
              {cartItems}
              <hr style={{ color: "black" }} />
              <div>
                <span style={{ float: "right" }}>
                  <strong style={{ color: "white" }}>Total : ${total}</strong>
                </span>
              </div>
              <NumPad.Number
                onChange={value => {
                  console.log("value", value);
                }}
                label={"Pay"}
                placeholder={"my placeholder"}
                value={100}
                decimal={2}
              />
            </ul>

            {/* sidebar */}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  product: state.product
});

export default connect(
  mapStateToProps,
  { getProducts, setDataToState }
)(Sell);
