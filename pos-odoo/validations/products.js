const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateProductInput(data) {
  let errors = {};

  data.name = !isEmpty(data.name) ? data.name : "";
  data.salesPrice = !isEmpty(data.salesPrice) ? data.salesPrice : "";
  data.barCode = !isEmpty(data.barCode) ? data.barCode : "";

  if (Validator.isEmpty(data.name)) {
    errors.name = "Product Name is required";
  }
  if (Validator.isEmpty(data.salesPrice)) {
    errors.salesPrice = "Product price is required";
  }
  if (Validator.isEmpty(data.barCode)) {
    errors.barCode = "BarCode  is required";
  }

  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};
