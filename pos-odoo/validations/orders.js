const Validator = require("validator");
const isEmpty = require("./is-empty");

module.exports = function validateCategoryInput(data) {
  let errors = {};

  // data.firstName = !isEmpty(data.firstName) ? data.firstName : "";
  // data.lastName = !isEmpty(data.lastName) ? data.lastName : "";
  // data.email = !isEmpty(data.email) ? data.email : "";
  // data.phone = !isEmpty(data.phone) ? data.phone : "";
  // // data.company = !isEmpty(data.company) ? data.company : "";
  // // data.country = !isEmpty(data.country) ? data.country : "";
  // // data.city = !isEmpty(data.city) ? data.city : "";
  // data.zipCode = !isEmpty(data.zipCode) ? data.zipCode : "";
  // data.address1 = !isEmpty(data.address1) ? data.address1 : "";
  // // data.address2 = !isEmpty(data.address2) ? data.address2 : "";
  // data.cardNumber = !isEmpty(data.cardNumber) ? data.cardNumber : "";
  // data.CVC = !isEmpty(data.CVC) ? data.CVC : "";

  // if (Validator.isEmpty(data.firstName)) {
  //   errors.firstName = "First Name is required";
  // }
  // if (Validator.isEmpty(data.lastName)) {
  //   errors.lastName = "Last Name is required";
  // }
  // if (Validator.isEmpty(data.email)) {
  //   errors.email = "Email is required";
  // }
  // if (Validator.isEmpty(data.phone)) {
  //   errors.phone = "Phone is required";
  // }
  // if (Validator.isEmpty(data.company)) {
  //   errors.company = "Company is required";
  // }
  // if (Validator.isEmpty(data.country)) {
  //   errors.country = "Country is required";
  // }
  // if (Validator.isEmpty(data.city)) {
  //   errors.city = "City is required";
  // }
  // if (Validator.isEmpty(data.zipCode)) {
  //   errors.zipCode = "ZipCode is required";
  // }
  // if (Validator.isEmpty(data.address1)) {
  //   errors.address1 = "Address1 is required";
  // }
  // // if (Validator.isEmpty(data.address2)) {
  // //   errors.address2 = "Address2 is required";
  // // }
  // if (Validator.isEmpty(data.cardNumber)) {
  //   errors.cardNumber = "Credit Card Number is required";
  // }
  // if (Validator.isEmpty(data.CVC)) {
  //   errors.CVC = "CVC is required";
  // }

  if (data.products == undefined || data.products == null) {
    errors.products = "Please Select some products";
  }
  // if (data.products.length == 0) {
  //   errors.products = "Please Select some products";
  // }
  return {
    errors: errors,
    isValid: isEmpty(errors)
  };
};
