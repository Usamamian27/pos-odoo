const Product = require("../schema/Products");
// var qr = require("qr-image");
// var JsBarcode = require("jsbarcode");
// var Canvas = require("canvas");
// var { createCanvas } = require("canvas");
const validateProductInput = require("../validations/products");
const isEmpty = require("../validations/is-empty");
class ProductController {
  AddProduct(req, res) {
    let { errors, isValid } = validateProductInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errors: errors });
    }
    // var qr_svg = qr.image("I love QR!", { type: "svg" });
    // console.log("qr-image", qr_svg);
    // qr_svg.pipe(require("fs").createWriteStream("i_love_qr.svg"));

    // var svg_string = qr.imageSync("I love QR!", { type: "svg" });
    // console.log("qr-string", svg_string);
    // var canvas = new Canvas();
    // var canvas = createCanvas();
    // JsBarcode(canvas, "Hello");

    // console.lof(canvas);

    Product.findOne({ name: req.body.name }).then(products => {
      if (products) {
        errors.productName = "Product Name Already Exists";
        return res.status(400).json({ errors: errors });
      } else {
        const newProduct = new Product({
          name: req.body.name,
          salesPrice: req.body.salesPrice,
          productType: req.body.productType,
          category: req.body.category,
          barCode: req.body.barCode,
          expiryDate: req.body.expiryDate,
          photoUrl: req.body.photoUrl
        });
        newProduct
          .save()
          .then(products => {
            return res.status(200).json(products);
          })
          .catch(err => res.status(400).json({ errors: errors }));
      }
    });
  }

  getAllProducts(req, res) {
    let errors = {};
    errors.products = "No Products found";
    Product.find()
      .sort({ date: -1 })
      .then(catalog => {
        if (catalog.length > 0) {
          res.status(200).json(catalog);
        } else {
          res.status(200).json({ errors: errors });
        }
      })
      .catch(errors => res.status(404).json({ errors: errors }));
  }

  // get products by category
  getProductByCategory(req, res) {
    let errors = {};
    errors.products = "No Products found";
    let id = req.params.cat_id;
    Product.find({ category: id })
      .then(result => {
        res.status(200).json(result);
      })
      .catch(error => {
        res.status(400).json({ errors: errors });
      });
  }

  //get user by Id
  getProductById(req, res) {
    let errors = {};
    errors.products = "No Products found";

    let id = req.params.id;
    Product.findById(id)
      .then(result => {
        res.status(200).json(result);
      })
      .catch(err => res.status(404).json({ errors: errors }));
    // if (products == null) {
    //   res
    //     .status(404)
    //     .json({ message: "Product against this ID isn't found" });
    // } else {
    //   res.status(200).json(products);
    // }
  }

  //get user and update
  updateProductById(req, res) {
    let id = req.params.id;
    let name = req.body.name;
    let salesPrice = req.body.salesPrice;
    let barCode = req.body.barCode;
    let productType = req.body.productType;
    let photoUrl = req.body.photoUrl;

    let body = {
      name: name,
      salesPrice: salesPrice,
      barCode: barCode,
      productType: productType,
      photoUrl: photoUrl
    };
    console.log("body", body);
    Product.findByIdAndUpdate(
      id,
      { $set: body },
      { new: true, useFindAndModify: false }
    )
      .then(products => res.status(200).json(products))
      .catch(err => res.status(404).json({ errors: err }));
  }
  //delete user
  async deleteProduct(req, res) {
    let errors = {};
    let msg = "error in deleting product.. Try Again";
    errors.msg = msg;
    let id = req.params.id;
    let product = await Product.findByIdAndDelete(id)
      .then(user => res.status(200).json(user))
      .catch(err => res.status(404).json({ errors: errors }));
  }
  
  // search method wrt product name
  searchProduct(req, res) {
    let sa = req.params.sad;
    sa.toLowerCase();

    Product.find(
      {
        name: {
          $regex: new RegExp(sa, "i")
        }
      },

      function(err, data) {
        res.json(data);
      }
    );
  }

  searchProductByBarCode(req, res) {
    let sa = req.params.bar;
    // sa.toLowerCase();
    console.log("sa===>", sa);
    let errors = {};

    Product.findOne(
      {
        barCode: {
          $regex: new RegExp(sa, "i")
        }
      },

      function(err, data) {
        if (data) {
          res.json(data);
        } else if (err) {
          res.json(err);
        }
      }
    );
  }
}
const productController = new ProductController();
module.exports = productController;
