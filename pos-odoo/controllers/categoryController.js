const Category = require("../schema/Category");

const validateCategoryInput = require("../validations/category");

class CategoryController {
  AddCategory(req, res) {
    let { errors, isValid } = validateCategoryInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errors: errors });
    }

    Category.findOne({
      name: req.body.name
    }).then(catalog => {
      if (catalog) {
        errors.category = "Category Name Already Exists";
        return res.status(400).json({ errors: errors });
      } else {
        const newCategory = new Category({
          name: req.body.name,
          photoUrl: req.body.photoUrl
        });

        newCategory
          .save()
          .then(catalog => {
            return res.status(200).json(catalog);
          })
          .catch(errors => res.status(400).json({ errors: errors }));
      }
    });
  }

  getAllCategory(req, res) {
    let errors = {};
    let category = "No Category found";
    errors.category = category;
    Category.find()
      .then(catalog => {
        if (catalog.length > 0) {
          res.status(200).json(catalog);
        } else {
          res.status(200).json({ errors: errors });
        }
      })
      .catch(errors => res.status(404).json({ errors: errors }));
  }

  //get user by Id

  getCategoryById(req, res) {
    try {
      let id = req.params.id;
      let catalog = Category.findById(id);
      if (catalog == null) {
        res
          .status(404)
          .json({ message: "Category against this ID isn't found" });
      } else {
        res.status(200).json(catalog);
      }
    } catch (error) {
      res.send(error);
    }
  }

  //get user and update
  async updateCategoryById(req, res) {
    try {
      let id = req.params.id;
      let name = req.body.name;
      let photoUrl = req.body.photoUrl;
      let subcat = [];
      subcat.push({ subName: req.body.subName });

      let subCategory = subcat;
      let body = {
        name: name,
        photoUrl: photoUrl,
        subCategory: subCategory
      };
      let catalog = await Category.findOneAndUpdate(
        id,
        { $set: body },
        { new: true }
      )
        .then(catalog => res.status(200).json(catalog))
        .catch(err => res.send(err));
    } catch (error) {
      res.send(error);
    }
  }
  //delete cat
  async deleteCategory(req, res) {
    let errors = {};
    let msg = "error in deleting category.. Try Again";
    errors.msg = msg;
    let id = req.params.id;
    let catalog = await Category.findByIdAndDelete(id)
      .then(cat => res.status(200).json(cat))
      .catch(err => res.status(404).json({ errors: errors }));
  }
}
const categoryController = new CategoryController();
module.exports = categoryController;
