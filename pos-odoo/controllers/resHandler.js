module.exports = (res, status, message, data) => {
  res.status(status).send({ message: message, ...data });
};
