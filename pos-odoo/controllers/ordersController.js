const Order = require("../schema/Orders");
const validateOrderInput = require("../validations/orders");

class OrdersController {
  // create an order
  PlaceOrder(req, res) {
    let { errors, isValid } = validateOrderInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errros: errors });
    }

    const newOrder = new Order({
      addedByUser: req.user.id,
      products: req.body.products,
      status: req.body.status,
      amountPaid: req.body.amountPaid,
      totalBill: req.body.totalBill
      // firstName: req.body.firstName,
      // lastName: req.body.lastName,
      // email: req.body.email,
      // phone: req.body.phone,
      // company: req.body.company,
      // country: req.body.country,
      // city: req.body.firstName,
      // zipCode: req.body.zipCode,
      // address1: req.body.address1,
      // address2: req.body.address2,
      // cardNumber: req.body.cardNumber,
      // CVC: req.body.CVC
    });
    newOrder
      .save()
      .then(orders => {
        return res.status(200).json(orders);
      })
      .catch(err => res.status(400).json({ errors: errors }));
  }
  // get all orders
  getAllOrders(req, res) {
    let errors = {};
    errors.orders = "No Orders found";
    Order.find()
      .sort({ date: -1 })
      .then(catalog => {
        if (catalog.length > 0) {
          res.status(200).json(catalog);
        } else {
          res.status(200).json({ errors: errors });
        }
      })
      .catch(errors => res.status(404).json({ errors: errors }));
  }

  //get order by Id
  getOrderById(req, res) {
    let errors = {};
    let msg = "error in order id .. Try Again";
    errors.msg = msg;
    let id = req.params.id;
    let orders = Order.findById(id);
    if (orders == null) {
      res.status(404).json({ errors: errors });
    } else {
      res.status(200).json(orders);
    }
  }

  //get order by Customer Id
  getOrderByCustomerId(req, res) {
    Order.find({ addedByUser: req.user.id })
      .then(orders => {
        res.status(200).json(orders);
      })
      .catch(error => res.status(404).json({ errors: error }));
  }

  //get order and update
  updateOrderById(req, res) {
    let products = req.body.products;
    console.log("update", req.params.id, req.body.products);
    let { errors, isValid } = validateOrderInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errros: errors });
    }
    let id = req.params.id;
    // let status = req.body.status;

    // let firstName = req.body.firstName;
    // let lastName = req.body.lastName;
    // let email = req.body.email;
    // let phone = req.body.phone;
    // let company = req.body.company;
    // let country = req.body.country;
    // let city = req.body.firstName;
    // let zipCode = req.body.zipCode;
    // let address1 = req.body.address1;
    // let address2 = req.body.address2;
    // let cardNumber = req.body.cardNumber;
    // let CVC = req.body.CVC;
    console.log("products", products);
    let body = {
      products: products
      // firstName: firstName,
      // lastName: lastName,
      // email: email,
      // phone: phone,
      // company: company,
      // country: country,
      // city: firstName,
      // zipCode: zipCode,
      // address1: address1,
      // address2: address2,
      // cardNumber: cardNumber,
      // CVC: CVC
    };
    console.log("body", req.body.products);
    Order.findOneAndUpdate(
      id,
      { $set: { products: req.body.products } },
      {
        new: true
        // returnNewDocument: true,
        // useFindAndModify: false
      }
    )
      .then(order => res.status(200).json(order))
      .catch(err => res.status(404).json({ errors: errors }));
  }
  //delete order
  async deleteOrder(req, res) {
    let errors = {};
    let msg = "error in deleting order.. Try Again";
    errors.msg = msg;
    let id = req.params.id;
    let product = await Order.findByIdAndDelete(id)
      .then(user => res.status(200).json(user))
      .catch(err => res.status(404).json({ errors: errors }));
  }
}
const ordersController = new OrdersController();
module.exports = ordersController;
