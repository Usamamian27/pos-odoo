const User = require("../schema/Users");
const keys = require("../config/keys");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const resHandler = require("./resHandler");
const validateLoginInput = require("../validations/login");
const validateRegisterInput = require("../validations/register");

class UserContoller {
  getAllUsers(req, res) {
    User.find()
      //   .then(user => res.status(200).json(user))
      //   .catch(err => res.status(404).json({ errors: "No user found" }));
      .then(user => res.status(200).json(user))
      .catch(err => resHandler(res, 404, false, { message: "No users found" }));
  }

  Login(req, res) {
    const { errors, isValid } = validateLoginInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errors: errors });
    }
    const email = req.body.email;
    const password = req.body.password;

    // Find user by Email
    User.findOne({ email: email }).then(admin => {
      // Check for admin
      if (!admin) {
        errors.email = "User not found";
        return res.status(404).json({ errors: errors });
      }

      // Check  password
      bcrypt.compare(password, admin.password).then(isMatch => {
        if (isMatch) {
          // User matched
          // Creating Jwt payload
          const payload = {
            id: admin.id,
            name: admin.name
          };

          // sign token
          jwt.sign(
            payload,
            keys.SecretOrKey,
            { expiresIn: 3.154e7 },
            (err, token) => {
              res.json({
                success: true,
                token: "Bearer " + token
              });
            }
          );
        } else {
          errors.password = "Incorrect Password";
          return res.status(404).json({ errors: errors });
          // return resHandler(res, 404, false, { errors: "Password Incorrect" });
        }
      });
    });
  }

  //   //get user by Id

  //   async getUserById(req, res) {
  //     try {
  //       let id = req.params.id;
  //       let user = await Admin.findById(id).populate({
  //         path: "role",
  //         populate: {
  //           path: "permissions",
  //           model: "permissions"
  //         }
  //       });
  //       if (user == null) {
  //         res.status(404).json({ message: "Data against this ID isn't found" });
  //       } else {
  //         res.status(200).json(user);
  //       }
  //     } catch (error) {
  //       res.send(error);
  //     }
  //   }

  //   //get user and update
  //   async updateUserById(req, res) {
  //     try {
  //       let id = req.params.id;
  //       let name = req.body.name;

  //       let body = {
  //         name: name
  //       };
  //       let admin = await Admin.findOneAndUpdate(
  //         id,
  //         { $set: body },
  //         { new: true }
  //       )
  //         .then(user => res.status(200).json(user))
  //         .catch(err => res.send(err));
  //     } catch (error) {
  //       res.send(error);
  //     }
  //   }

  //   //get user and update password
  //   async changepassword(req, res) {
  //     try {
  //       let id = req.params.id;
  //       let password = req.body.password;
  //       password = bcrypt.hashSync(password, 10);
  //       let user = await Admin.findOneAndUpdate(
  //         { _id: id },
  //         { password: password },
  //         {
  //           new: true
  //         }
  //       );
  //       res.status(200).send(user);
  //     } catch (error) {
  //       res.send({ error: "error in updating password" });
  //     }
  //   }
  //   //delete user
  //   async deleteUser(req, res) {
  //     try {
  //       let id = req.params.id;
  //       let admin = await Admin.findByIdAndDelete(id);
  //       res.send(admin);
  //     } catch (error) {
  //       res.status(400).json(error);
  //     }
  //   }

  async SignUp(req, res) {
    let { errors, isValid } = validateRegisterInput(req.body);

    // Check  Validations

    if (!isValid) {
      return res.status(400).json({ errors: errors });
    }

    User.findOne({ email: req.body.email }).then(admin => {
      if (admin) {
        errors.email = "Email Already Exists";

        return res.status(400).json({ errors: errors });
      } else {
        const newAdmin = new User({
          firstName: req.body.firstName,
          lastName: req.body.lastName,
          email: req.body.email,
          password: req.body.password
        });

        newAdmin
          .save()
          .then(admin => res.status(200).json(admin))
          .catch(err => res.status(404).json({ errors: errors }));
      }
    });
  }
  //delete user
  async deleteUser(req, res) {
    let errors = {};
    let msg = "error in deleting user.. Try Again";
    errors.msg = msg;
    let id = req.params.id;
    let admin = await User.findByIdAndDelete(id)
      .then(user => res.status(200).json(user))
      .catch(err => res.status(404).json({ errors: errors }));
  }
}
const userContoller = new UserContoller();
module.exports = userContoller;
