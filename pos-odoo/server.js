const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const passport = require("passport");
const app = express();
const path = require("path");
app.use(bodyParser.json());
const api = require("./routes/api");

app.use(
  cors({
    origin: "*",
    credentials: true
  })
);
// Body parser middleWare
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use("/uploads", express.static(path.join(__dirname, "uploads")));
// DB Config
const db = require("./config/keys").mongoURI;
/// Connect to mongo Db

mongoose
  .connect(db, { useNewUrlParser: true, useCreateIndex: true })
  .then(() => {
    console.log("Connection to MongoDB :  Succesful");
  })
  .catch(err => console.log(err));

// Passport middleWare
app.use(passport.initialize());
// passport config
require("./middleware/passport")(passport);

app.use("/api", api);

app.get("/", (req, res) => {
  res.send("api is Connected Kindly Do your Work");
});
app.use(express.static(path.join(__dirname, "/build")));

app.get("/*", (req, res) => {
  res.sendFile(path.join(__dirname + "/build/index.html"));
});

// server port integration
const port = process.env.PORT || 5000;
app
  .listen(port, () => {
    console.log(`Server is listening at ${port}`);
  })
  .on("error", () => console.log(`error in listening at ${port}`));
