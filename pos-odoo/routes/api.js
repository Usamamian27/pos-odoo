const { Router } = require("express");
const passport = require("passport");
const multer = require("multer");

//router
const router = new Router();

// controllers
const userController = require("../controllers/userController");
const categoryController = require("../controllers/categoryController");
const productController = require("../controllers/productController");
const orderController = require("../controllers/ordersController");

// Users CRUD
router.get("/test", (req, res) => {
  res.json("good to go");
});
router.post("/users/register", userController.SignUp);
router.post("/users/login", userController.Login);
router.get("/users", userController.getAllUsers);
router.delete("/users/:id", userController.deleteUser);

// Category CRUD
router.get("/categories", categoryController.getAllCategory);
router.post("/category/create", categoryController.AddCategory);

// Products CRUD
router.get("/products", productController.getAllProducts);
router.post("/product/create", productController.AddProduct);
router.delete("/product/:id", productController.deleteProduct);
router.get("/get-product/:id", productController.getProductById);
router.post("/product/update/:id", productController.updateProductById);
router.get("/search/product/:sad", productController.searchProduct);
router.get("/barcode/search/:bar", productController.searchProductByBarCode);

// Orders CRUD
router.get("/orders", orderController.getAllOrders);
router.post(
  "/order/create",
  passport.authenticate("user", { session: false }),
  orderController.PlaceOrder
);
router.delete(
  "/order/:id",
  passport.authenticate("user", { session: false }),
  orderController.deleteOrder
);
router.put(
  "/order/:id",
  passport.authenticate("user", { session: false }),
  orderController.updateOrderById
);

// Uploader
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "uploads");
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + file.originalname);
  }
});

var upload = multer({ storage: storage });
//   router.get("/landing", function (req, res) {
//     res.sendFile(__dirname + "/index.html");
//   });

// upload single file

router.post("/uploadfile", upload.single("myFile"), (req, res, next) => {
  const file = req.file;
  let errors = {};
  errors.msg = "Please Upload a file";
  if (!file) {
    // const error = new Error("Please upload a file");
    // error.httpStatusCode = 400;
    // return next(error);
    return res.status(404).json({ errors: errors });
  } else {
    return res.send({
      photoUrl: `${req.protocol + "://" + req.get("host")}/uploads/${
        req.file.filename
      }`
    });
  }
});
//Uploading multiple files
router.post(
  "/uploadmultiple",
  upload.array("myFiles", 12),
  (req, res, next) => {
    const files = req.files;
    if (!files) {
      const error = new Error("Please choose files");
      error.httpStatusCode = 400;
      return next(error);
    }

    var arr = [];
    for (let index = 0; index < files.length; index++) {
      arr.push(
        `${req.protocol + "://" + req.get("host")}/uploads/${
          req.files[index].filename
        }`
      );
    }

    res.send(arr);
  }
);

module.exports = router;
