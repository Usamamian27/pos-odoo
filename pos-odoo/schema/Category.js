const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema

const CategorySchema = new Schema({
  name: {
    type: String,
    required: true
  },

  photoUrl: [String],
  addedByUser: {
    type: String
  },
  date: {
    type: String,
    default: Date(Date.now)
  }
});

module.exports = Category = mongoose.model("category", CategorySchema);
