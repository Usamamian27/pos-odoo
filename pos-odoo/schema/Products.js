const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema

const ProductsSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  salesPrice: {
    type: Number,
    required: true
  },
  //   size: {
  //     type: String,
  //     required: true
  //   },
  //   stock: {
  //     type: Number,
  //     required: true
  //   },

  productType: {
    type: String
  },
  barCode: {
    type: String
  },
  //   addedByUser: {
  //     type: String
  //   },
  photoUrl: [String],
  category: {
    type: Schema.Types.ObjectId,
    ref: "category"
  },
  expirayDate: {
    type: String
  },

  date: {
    type: String,
    default: Date(Date.now)
  }
});

ProductsSchema.pre("find", function() {
  const product = this;
  product.populate("category");
});
ProductsSchema.pre("findById", function() {
  const product = this;
  product.populate("category");
});
module.exports = Products = mongoose.model("products", ProductsSchema);
