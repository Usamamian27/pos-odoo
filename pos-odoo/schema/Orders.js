const mongoose = require("mongoose");
const Schema = mongoose.Schema;
// Create Schema

const OrdersSchema = new Schema({
  order_status: {
    type: String,
    default: "pending"
  },
  totalBill: {
    type: Number,
    default: 0
  },
  amountPaid: {
    type: Number,
    default: 0
  },

  products: [
    {
      productQuantity: {
        type: Number
      },
      // measurements
      name: {
        type: String
      },
      salesPrice: {
        type: Number
      },
      productType: {
        type: String
      },
      barCode: {
        type: String
      },
      photoUrl: [String],
      category: {
        type: Schema.Types.ObjectId,
        ref: "category"
      }
    }
  ],

  addedByUser: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  // shipment and checkout details
  // firstName: {
  //   type: String,
  //   required: true
  // },
  // lastName: {
  //   type: String,
  //   required: true
  // },
  // email: {
  //   type: String,
  //   required: true
  // },
  // phone: {
  //   type: String,
  //   required: true
  // },
  // company: {
  //   type: String
  //   // required: true
  // },
  // country: {
  //   type: String
  //   // required: true
  // },
  // city: {
  //   type: String
  //   // required: true
  // },
  // zipCode: {
  //   type: String
  //   // required: true
  // },
  // address1: {
  //   type: String,
  //   required: true
  // },
  // address2: {
  //   type: String
  //   // required: true
  // },
  // cardNumber: {
  //   type: String,
  //   required: true
  // },
  // CVC: {
  //   type: String,
  //   required: true
  // },
  // customer: {
  //   type: Schema.Types.ObjectId,
  //   ref: "customers"
  // },
  date: {
    type: String,
    default: Date(Date.now)
  }
});

// OrdersSchema.pre("find", function() {
//   const order = this;
//   order.populate("customer").populate({
//     path: "products.ordered",
//     model: "products"
//   });
// });

module.exports = Products = mongoose.model("orders", OrdersSchema);
